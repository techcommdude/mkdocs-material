
This project uses a GitLab CI/CD pipeline to publish the latest version of the documentation to this GitLab Pages URL:

* [https://techcommdude.gitlab.io/mkdocs-material](https://techcommdude.gitlab.io/mkdocs-material)

**Note**:  A commit to the `Main` branch of this repository will trigger a build that updates the website at the
URL mentioned above.

This repository is a Docs-as-Code proof-of-concept using [MkDocs](https://www.mkdocs.org/) for the documentation, [Material](https://squidfunk.github.io/mkdocs-material/) for the theme/customization and GitLab for the CI/CD pipeline.

Here's a screen capture of the latest version of the documentation:

![Engineering documentation for Project Horizon](Project_Horizon_Docs.png)
