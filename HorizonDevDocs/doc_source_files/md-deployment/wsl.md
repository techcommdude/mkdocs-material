
WSL allows you to control the memory configuration for Docker Desktop from within Windows. 

If you need more information on WSL, you can refer to the official Microsoft documentation: 

* [https://learn.microsoft.com/en-us/windows/wsl/](https://learn.microsoft.com/en-us/windows/wsl/){:target="_blank"}

### Installing WSL on Windows

You must have a version 2 installation of WSL on your system. To confirm the version of WSL, run the `wsl -l -v` command in your terminal. If you have version 2 installed, you will see this output that states the version of WSL:

![](../images/wsl-version.png)

The `wsl -l -v` command will also tell you if WSL is running:

![](../images/wsl-running.png)

If you need to install version 2 of WSL or don't have WSL installed, see this link for more information on installing WSL on Windows:

* [https://learn.microsoft.com/en-us/windows/wsl/install](https://learn.microsoft.com/en-us/windows/wsl/install){:target="_blank"}

### Memory allocation for Docker Desktop using the *.wslconfig* file

Docker within the context of a local deployment of Project Horizon has specific memory requirements. It's often helpful to use a Linux distribution such as WSL to manage the memory for Docker within Windows. WSL can dynamically allocate memory on an as-needed basis and release memory when it's no longer required. With WSL version 2, you can either create or configure the *.wslconfig* file to manage the memory for Docker. This topic has an example of how you can configure this file for Docker.

**Steps**:

1. Open a text editor and create or edit the *.wslconfig* file in your user home directory which is typically located at `C:\Users\<YourUsername>\.wslconfig`. If the file doesn't exist, you can create it.

    !!! note

        You can determine your home directory with the `cd~` command in your terminal or `echo %USERPROFILE%` in your Windows command prompt.

2. Use this configuration in your *.wslconfig* file. This configuration has been refined and tested by Project Horizon team members and offers stability and exceptional performance:

    ```
    [wsl2]
    # Limits VM memory to use no more than 48 GB, defaults to 50% of RAM
    memory=28GB
    # Sets the amount of swap storage space to 12GB, default is 25% of available RAM
    swap=56GB
    # Sets the VM to use 8 virtual processors
    processors=8
    localhostForwarding=true

    ```

    !!! note

        More information on this configuration can be found [here](https://learn.microsoft.com/en-us/windows/wsl/wsl-config#configure-global-options-with-wslconfig){:target="_blank"}.

3. To ensure WSL is using the new memory configuration, restart the WSL instance by opening a PowerShell terminal window and running this command. This will ensure that WSL is using the new memory configuration the next time it starts:

    ```
    wsl --shutdown
    ```

4. You may see this message in Windows, but you can click **Restart** to restart WSL:

    ![Restart WSL](../images/wsl-restart.png)

5. You can also monitor the *Vmmem* (Virtual machine memory) process in your Windows Task Manager that specifies how much memory Docker and Kubernetes cluster is currently using through WSL:

    ![](../images/docker-memory.png)