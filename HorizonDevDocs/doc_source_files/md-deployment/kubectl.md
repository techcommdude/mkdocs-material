
<!--  TODO: Used for Google Cloud GKE, have more info in notes. -->

kubectl is commonly used in Google Cloud Platform (GCP) for managing Kubernetes clusters. Kubernetes is an open-source container orchestration platform, and Google Kubernetes Engine (GKE) is a managed Kubernetes service provided by Google Cloud.

kubectl is the command-line tool used to interact with Kubernetes clusters, including those deployed on GCP using GKE. You can use kubectl to perform various tasks such as deploying and managing containerized applications, scaling workloads, inspecting cluster resources, and more on GKE clusters.

### Download and install kubectl

<!--  TODO: Email to Alek about having more than one version of kubectl on machine. One for Docker, one for GCP.  Provide guidance on this -->

!!! note

    You must use a kubectl version that is within one minor version difference of your cluster. For example, a v1.28 client can communicate with v1.27, v1.28, and
    v1.29 control planes. Using the latest compatible version of kubectl helps avoid unforeseen issues.

This topic provides information on how to install and validate the kubectl installation on your local machine. You can find information on the latest stable version [here](https://dl.k8s.io/release/stable.txt){:target="_blank"}.

**Steps**:

1. You can download the latest stable version from here:

    * [https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/](https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/){:target="_blank"}

2. Append or prepend the _kubectl.exe_ binary folder to your PATH environment variable:

    ![](../images/kubectl-path.png)

3. Check that your installation is successful with the `kubectl version --client` command in your terminal:

    ![](../images/kubectl-client-version.png)

4. You can use the `kubectl version` command in your terminal to display the client and server version of Kubernetes:

    ![](../images/kubectl-version.png)

### Configure kubectl to use a remote Kubernetes cluster

<!--  TODO: [Email to Alek about having more than one version of kubectl on machine. One for Docker, one for GCP.  Provide guidance on this](https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/#verify-kubectl-configuration)) -->

You will require a configuration file to connect to the GKE cluster in GCP.