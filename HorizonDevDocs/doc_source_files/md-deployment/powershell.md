
PowerShell is a useful tool that is used for many of the scripts provided with the local and cloud-based demo deployments. You will require PowerShell version 7.3 or greater.

Typically, the default installation of PowerShell on a Windows system will be version 5.2 or lower, and 
therefore you will need to upgrade. After upgrading to PowerShell 7.3+, you can point your IDE to
the new version of PowerShell. This is usually configured in the settings/preferences of the IDE.

To install PowerShell 7.3 or greater alongside an existing installation of PowerShell, you can follow these steps.

PowerShell 7.3+ is a newer, cross-platform version of PowerShell and can coexist with previous versions like PowerShell 5.1.

!!! note

    These instructions assume you are using a Windows-based system. If you are using a 
    different operating system, the installation process may be different.


### Installing PowerShell 7.3 or higher

1. Before installing PowerShell 7.3, you should check the currently installed version to ensure you aren't already at version 7.3. You can do this by opening a PowerShell terminal and running the `$PSVersionTable` command.

    Your output will look something like this:

    ![](../images/PS-version-table.png)

2. In this case, the user needs to install a more recent version of PowerShell. If you don't have the appropriate version installed, you can go to this page and download the stable version: [https://github.com/PowerShell/PowerShell#get-powershell](https://github.com/PowerShell/PowerShell#get-powershell){:target="_blank"}

    ![](../images/PS-version-stable.png)

2. The installation will prompt you to select the installation location. To avoid conflicts with your existing PowerShell installation, it's a good practice to install the new version of PowerShell in a separate directory. For example, you can select a directory something like this: `C:\Program Files\PowerShell\7`.

    !!! note

        The installation also includes steps to add PowerShell to your _Path_ environment variable.  Do this if
        you want to run PowerShell from the command line.

3. Verify your installation once again at the command line:

    ![](../images/pwsh.png)

    Now you have PowerShell 7.3 coexisting with your existing PowerShell installation. You can use both versions as needed, and they won't interfere with each other.

!!! tip

    After installing PowerShell, you can also consider using Windows Terminal to access many of the shells on your system (PowerShell, Bash, etc.).  For more general information on Windows Terminal, see this [topic](https://softwarekeep.com/help-center/powershell-terminal-command-prompt-windows).  For information on downloading and installing Windows Terminal, you can refer to this site: [https://github.com/microsoft/terminal](https://github.com/microsoft/terminal).
