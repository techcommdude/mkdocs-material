
As a first step, you will need to fork the GitLab repository that has the baseline Terraform templates. You can do this with the project forking workflow in GitLab. This topic has more details on the steps required.

**Steps**:

1. You will need to fork this GitLab repository: [https://gitlab.dematic.com/horizon/demo-project/mcf-demo/mcf-demo-dev-local](https://gitlab.dematic.com/horizon/demo-project/mcf-demo/mcf-demo-dev-local){:target="_blank"}

2. Fork the repository in GitLab using the project forking workflow. For more information on how to do this, see the GitLab documentation [here](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html){:target="_blank"}.

    ![Project forking workflow](../images/project-forking-workflow.png)

<!-- TODO: You can update the fork by doing this.  You don't necessarily want to push your changes to the upstream repository, but you can get updates from the upstream repository:  https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#update-your-fork -->