
For local deployments (using the provided demo or your own project), you can configure Docker Desktop to use a Kubernetes single-node cluster on your machine. 

### Acquiring a Docker Desktop license

You must be a member of the **dematiccorp** group within Docker to install and use Docker Desktop on your system.          

To request a license, you can log a request here in Dematic JIRA:

* [https://jira.dematic.com/servicedesk/customer/portal/2](https://jira.dematic.com/servicedesk/customer/portal/2){:target="_blank"}

After approving your request, you will receive an email invitation to join the group.

### Downloading Docker Desktop

After acquiring your license, you can download Docker Desktop here for Windows:

* [https://docs.docker.com/desktop/install/windows-install/](https://docs.docker.com/desktop/install/windows-install/){:target="_blank"}

### Docker configuration for a Kubernetes cluster

This topic outlines how to configure Docker to use a Kubernetes single-node cluster. This type of cluster is most often used for testing purposes in local deployments.

!!! tip

    The [kind](https://kubernetes.io/docs/tasks/tools/#kind){:target="_blank"} and [minikube](https://kubernetes.io/docs/tasks/tools/#minikube){:target="_blank"} tools are not required to use Docker Desktop with Kubernetes in local deployments.


**Steps**:

1. After opening Docker Desktop and signing in to your account, select the gear icon for settings and display this page.

    ![](../images/kubernetes-enable-1.png)

2. Select **Enable Kubernetes** and click **Apply and Restart**.
3. The local single-node Kubernetes cluster will then start:

    ![](../images/kubernetes-enable-2.png)

4. Docker Desktop will display a green icon if the Kubernetes cluster has successfully started:

    ![](../images/kubernetes-enable-3.png)


### Other required Docker Desktop settings

<!-- TODO: Add Troubleshooting information when I come across it -->

It's recommended that you select WSL 2 based engine for Docker as shown in the screen capture below. You can also choose to start Docker when you log in to the machine. However, you should keep in mind that this option will have an impact on your machine memory, and you should be mindful of this. You can always shut down Docker Desktop if it's using too much memory on a regular basis.

![](../images/wsl-2-docker.png)
