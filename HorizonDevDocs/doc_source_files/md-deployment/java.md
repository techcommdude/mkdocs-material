
Horizon requires Java to run the Maven build tool. Maven commands are used to build the warehouse and the Horizon UI.

As of writing, you can install the version of Java (Amazon Corretto 18) found at this URL:

* [https://github.com/corretto/corretto-18/releases](https://github.com/corretto/corretto-18/releases)

![](../images/java-jdk.png)

In a terminal session, use the `java -version` command to confirm a successful installation and configuration:

![](../images/java-version.png)

**Notes:**

* You should remember the installation location on your machine as you may need this information at a later point.
* Install the full SDK and update the *Path* and *JAVA_HOME* environment variable if it prompts you.

  ![](../images/java_home.png)

* As mentioned above, you should set your `PATH` environment variable as part of the installation, but you may need to do
  it manually.

  ![](../images/path-environment-variable.png)
