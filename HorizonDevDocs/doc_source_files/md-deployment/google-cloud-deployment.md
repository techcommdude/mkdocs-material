<!--  TODO: Add more overview information here. -->

!!! warning

    This section is currently under construction.

This section outlines how to deploy Horizon on Google Cloud using an existing demo project in GitLab.

## Clone or fork the GitLab repository

As a first step, you will need to clone or fork the GitLab repository that has the baseline Terraform templates. You can do this by cloning the project and then removing the Git history, or you can use the project forking workflow in GitLab. This topic has more details on both approaches.

## Updating the Terraform templates with PowerShell

To get the latest versions of the Terraform templates, you can run a PowerShell script that's found in the *terraform* directory of the project.

