
<!--  TODO: -->

!!! warning

    This section is currently under construction.

You will need to:

* Request access to GCP with a RemedyForce ticket.
* Install and configure the Google Cloud SDK.


### Request access to Dematic Google Cloud

<!-- TODO: According to this page: https://wiki.dematic.com/display/IQHI/Horizon+New+Team+Member+On-Boarding+Checklist, we may need access to gcp-hzn-developers
gcp-developers groups in Google Cloud.  Is that true for GSO?
 -->

A Dematic Google Cloud account will give you access to the Dematic Docker images in the Google Cloud registry. It will also give you the ability to deploy a customer project on Google Cloud.

**Steps**:

1. Create a [RemedyForce](https://dematic--bmcservicedesk.vf.force.com/apex/selfservicenew#/support/catalog/common){:target="_blank"} ticket requesting access. Mention the following in the ticket:

   * You will be working on Project Horizon and will require access to the appropriate resources. This will give you the ability to access the appropriate Docker images in the Google Cloud registry.

2. Once the ticket is resolved, Dematic IT will inform you that you now have a Google Cloud account, but may not give you credentials. However, your Google Cloud account is normally associated with your Dematic user account that begins with the letter 'A'. For example: *A0064314@dematic.com*. 

    !!! note

        As of writing, it is not likely that new accounts will use a Dematic email account with this pattern: *firstname.lastname@dematic.com*.

3. To verify that your account is working, you can log in to Google Cloud with your new Dematic credentials at [cloud.google.com](https://cloud.google.com){:target="_blank"}.

    !!! note

        If you are currently signed in with a personal Google account, you can switch to your Dematic Google account in your browser.

4. On the Google Cloud home page, click **Sign In** to specify your Dematic user account and then click **Next**:

    ![Google Cloud account](../images/google-account.png)

5. Specify your Dematic user account and click **Next** to start the authentication process:
   
    ![Sign-in Dematic](../images/sign-in-dematic.png)

6. After authentication is complete, you'll likely see this screen. Click **Continue**:
   
    ![Confirm Google Cloud account](../images/confirm-google-cloud.png)

7. You are now signed in to your Dematic Google Cloud account:

    ![Manage your Google Account](../images/manage-google-account.png)

8. You can confirm your access to Dematic resources in Google Cloud by selecting the **DEMATIC.COM** resource:

    ![Select resource in Google Cloud](../images/google-cloud-select-resource.png)

Also note that if you have an existing Google account, you will likely now have two accounts on your machine. If necessary, you may need to switch back and forth between these accounts:

![Gmail or Google Cloud](../images/gmail-google-cloud.png)

### Google Cloud SDK installation and configuration

!!! note

    This procedure assumes you have already created a Dematic Google Cloud account.

Please keep in mind the following sites during the installation and setup process for the Google Cloud SDK:

* **Admin Portal**—[https://admin.google.com](https://admin.google.com)
* **Console Portal**—[https://console.cloud.google.com](https://console.cloud.google.com)
* **Developers Portal**—[https://console.developers.google.com](https://console.developers.google.com)
* **Google Cloud home**—[https://cloud.google.com](https://cloud.google.com)

<!--  TODO: -->

The Google Cloud SDK includes the Google Cloud CLI, cloud client libraries, as well as extra tools and documentation. The SDK gives you access to all the Google Cloud tools and GCP services.