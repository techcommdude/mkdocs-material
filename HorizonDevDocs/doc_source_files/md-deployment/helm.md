

<!--  TODO: Any particular version?  Put this in the requirements. -->

Instructions on how to download and install Helm are [here](https://helm.sh/docs/intro/install/){:target="_blank"}.  You can download the latest from this GitHub repository: 

* [https://github.com/helm/helm/releases/latest](https://github.com/helm/helm/releases/latest){:target="_blank"}

For more information on Helm and Helm Charts, see the following entries in the Glossary:

* [Helm](../md-glossary/glossary.md#helm).
* [Helm chart](../md-glossary/glossary.md#helm-chart).

**Steps**:

1. Download and extract the zip to your local computer (from the link above) and add the Helm binary (_helm.exe_) to your _Path_ environment variable.

    ![Helm path](../images/helm-path.png)

2. Use the `helm version` command in your terminal shell to check that your Helm installation is successful. You may need to restart your shell:

    ![Helm version](../images/helm-version.png)

3. Open a PowerShell terminal window and add the **horizon** repository with the command received from Artifactory (see [this topic](#artifactory-access-and-configuration) for more information).  The command includes your Artifactory username and the token value for the password:

    ```
    helm repo add horizon https://artifactory.dematic.com/artifactory/api/helm/horizon-helm --username <artifactory-username> --password <artfifactory-helm-repository-token-value>
    
    ```

    In the example command above, replace the values in angled brackets (such as `<artifactory-username>` and `<artfifactory-helm-repository-token-value>`) with your values.

4. After executing the command in PowerShell, you should see output like this:

    ![](../images/horizon-helm.png)

5. The `helm repo list` command will confirm your Helm repositories:

    ![](../images/helm-repo-list.png)