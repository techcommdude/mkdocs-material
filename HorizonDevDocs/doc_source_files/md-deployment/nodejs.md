This is a general Dematic wiki page that contains good information on setting up the UI environment: 

* [https://wiki.dematic.com/display/IQH/Setting+up+the+UI+environment](https://wiki.dematic.com/display/IQH/Setting+up+the+UI+environment){:target="_blank"}

Steps:

1. You can download the appropriate version of node from this page: 
   
    [https://nodejs.org/en/download](https://nodejs.org/en/download){:target="_blank"}. You can also download previous versions.

2. The installation may do it for you, but you must add the installation directory to your *Path* environment variable:

    ![node installation path](../images/node-installation-path.png)

3. You must also ensure that npm (Node Package Manager) is in your user Path variable:

    ![npm user path](../images/npm-user-path.png)

4. Running `npm --version` will confirm that the installation was successful.


## NPM (Node Package Manager) repository installation and configuration

You will need to configure the `npm-virtual` repository in Artifactory to build and deploy the Horizon UI.

This Dematic wiki page contains additional information on how to do this: 

* [https://wiki.dematic.com/display/IQH/NPM+configuration](https://wiki.dematic.com/display/IQH/NPM+configuration){:target="_blank"}

**Steps**:

1. Log in to Artifactory here: [https://artifactory.dematic.com/ui/login/](https://artifactory.dematic.com/ui/login/){:target="_blank"}

2. Use the **Set Me Up** tool in the top-right of the page and select the npm package type:

    ![Artifactory Set Me Up](../images/npm-artifactory-set-me-up.png)
    ![npm package](../images/npm-artifactory-set-me-up-2.png)

3. On the **Configure** tab, enter your account password and click **Generate Token and Create Instructions**. Your output will look something like this:

    ![instructions](../images/npm-artifactory-set-me-up-3.png)

4. Copy the code at the bottom of the above screen capture, but exclude the leading `@<SCOPE>`. Your code will look something like this:

    ```
    registry=https://artifactory.dematic.com/artifactory/api/npm/npm-virtual/
    //artifactory.dematic.com/artifactory/api/npm/npm-virtual/:_password=<npm-virtual-acess-token>
    //artifactory.dematic.com/artifactory/api/npm/npm-virtual/:username=a0088315
    //artifactory.dematic.com/artifactory/api/npm/npm-virtual/:email=Geoffrey.Farnell@dematic.com
    //artifactory.dematic.com/artifactory/api/npm/npm-virtual/:always-auth=true    
    ```

5. Save the code you just copied to your local HOME directory in a file called `.npmrc`.
   
6. Confirm that your user configuration was successful by running the `npm config ls -l` command:

    ![npm registry](../images/npm-registry.png)

7. Verify that you can log in to the Artifactory repository with `npm login`:

    ![npm login](../images/npm-login.png)


