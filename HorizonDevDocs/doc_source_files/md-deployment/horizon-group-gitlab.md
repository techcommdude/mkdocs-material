
<!--  TODO: -->

You will require access to the **Horizon** group in GitLab. This group contains all Git repositories related to Project Horizon.

To download the required Horizon repositories, you have a few options.

**Steps**:

* You can request access to this group in GitLab [here](https://gitlab.dematic.com/horizon){:target="_blank"}.

    You will require a Dematic JIRA account to access this system.

* Alternatively, select **GitLab Group** in the DevOps support portal and request access: [https://jira.dematic.com/servicedesk/customer/portal/2](https://jira.dematic.com/servicedesk/customer/portal/2){:target="_blank"}
