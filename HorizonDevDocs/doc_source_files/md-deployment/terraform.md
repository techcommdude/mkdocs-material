
<!--  TODO: Refer to Hardware and Software Requirements for version?) -->

This topic outlines the Terraform installation process. You should install the latest stable version. For more information on Terraform and Terraform templates, see the [Glossary](../md-glossary/glossary.md#terraform).

**Steps**:

1. Download the zip and install Terraform from here: 

    * [https://developer.hashicorp.com/terraform/downloads](https://developer.hashicorp.com/terraform/downloads){:target="_blank"}

    ![](../images/terraform-install.png)

2. Place the extracted binary (_terraform.exe_) on your system and add it to your _PATH_ environment variable:

    ![](../images/terraform-install-path.png)

3. Verify the installation by running the `terraform -help` command:

    ```
    terraform -help

    ```

    ![](../images/terraform-help.png)
