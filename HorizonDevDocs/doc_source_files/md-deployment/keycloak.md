

<!--  FIXME: Update with Keycloak information-->

<!-- TODO: Notes on how to configure and use Keycloak can be added here. -->

[Keycloak](https://www.keycloak.org/){:target="_blank"} is an open source identity and access management tool for modern applications and services.

Users authenticate with Keycloak rather than individual applications. This means that your applications don't have to deal with login forms, authenticating users, and storing users. Once logged-in to Keycloak, users don't have to login again to access a different application.

This also applies to logout. Keycloak provides single-sign out, which means users only have to logout once to log out of all applications that use Keycloak.

For more information on Keycloak in the Project Horizon wiki, refer to this page: [https://wiki.dematic.com/display/IQH/Keycloak](https://wiki.dematic.com/x/n4iTKQ){:target="_blank"}

