You can use the latest supported build (in zip file format) for the Horizon MCF demo from this Dematic wiki page: 

* [https://wiki.dematic.com/pages/viewpage.action?pageId=553377813](https://wiki.dematic.com/pages/viewpage.action?pageId=553377813){:target="_blank"}

You can refer to this page for general configuration information. Note that not everything will apply to the current demo:

* [https://wiki.dematic.com/display/IQH/Setting+up+and+using+DVF+with+a+demo+project](https://wiki.dematic.com/display/IQH/Setting+up+and+using+DVF+with+a+demo+project){:target="_blank"}

When you run DVF, you'll notice if you zoom in on the PLC equipment indicator lights that most of them are currently red.  This is because you have not deployed the `Edge` application which will be done in the next step.

![DVF indicator lights are red](../images/dvf-plc-red.png)