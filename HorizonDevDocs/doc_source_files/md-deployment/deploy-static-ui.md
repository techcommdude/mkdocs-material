This section goes through the deployment of the static Horizon UI that is part of the demo project. It assumes that you have already installed node.js and configured the npm-virtual repository in Artifactory.

### Build the Horizon UI with Maven and npm

**Steps**:

1. In the root of the repository, run this command: `mvn clean install -Pui -pl ui -DskipTests -DnpmSkipTests`

    ![UI Maven build](../images/ui-build-maven.png)

3. Navigate to the `<my-git-repo-demo-dev-local>\ui` directory and run the `npm run update-scm-ui` command:

    ![npm run update-scm-ui command](../images/npm-update-scm-ui.png)

4. In the same directory, run the `npm run build` command:

    ![npm run build command](../images/npm-run-build.png)

### Deploy the Horizon UI to your Kubernetes cluster

**Steps**:

1. Navigate to `<my-git-repo-demo-dev-local>\terraform` directory
2. Run the `upload-ui.ps1` script in a terminal session:

    ![upload-ui.ps1 script](../images/upload-ui.png)

3. You can access the Horizon application at [https://host.docker.internal/](https://host.docker.internal/){:target="_blank"}. As of writing, you can use a username of `horizon` and a password of `P@ssw0rd`.












