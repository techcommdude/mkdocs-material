This section outlines the procedure for building the warehouse with Maven and then upload the warehouse to your cluster. You will also run scripts to create and activate SKUs.

### Generate the warehouse with Maven

This topic assumes you have already installed Maven and Java.

**Steps**:

1. Generate the warehouse with Maven by navigating to this directory which has the *pom.xml* file: 

    `<my-git-repo-demo-dev-local>\artifacts\whgeneration`

2. Then run this Maven command:

    `mvn clean install`

4. A successful build will look something like this: 
 
    ![Maven build warehouse](../images/maven-build-warehouse.png)

3. The build process will create a *target* directory in this location:

    `<my-git-repo-demo-dev-local>\artifacts\whgeneration\target`


### Port forwarding to your Kubernetes cluster

Port forwarding enables your local deployment to communicate with your Kubernetes cluster. You will have to port forward your `WHC`, `MDM` and `INB` Kubernetes pods. You can use a separate PowerShell window for each of the commands and the session must remain active to complete the deployment. Don't close the PowerShell windows during this process.

!!! note

    Port forwarding of the INB pod is not strictly necessary for the Terraform setup, but is required for downloading Advices and inducting a case for a DVF emulation.


This article in the Kubernetes documentation has more information on port forwarding: 

* [https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/](https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/){:target="_blank"}

#### Port forward for the WHC Kubernetes pod

Port forward instructions for the `WHC` pod in a PowerShell session.

**Steps**:

1. Use this command to list all pod names in the `central` namespace. You can then copy the pod name for WHC:

    ```
    kubectl get pods -n central

    ```
    ![kubectl get pods -n central](../images/get-pods-whc-1.png)

2. You can also use this command to find the specific pod name:

    ```
    kubectl get pods -n central --no-headers | Select-String '^whc' | ForEach-Object { $_.Line.Split()[0] }
    ```
    ![kubectl get pods -n central....](../images/get-pods-whc-2.png)

3. Issue this command to forward the port for your deployment:

    ```
    kubectl port-forward -n central <whc-pod-name> 12280:8080
    ```

4. In this example you would use this command:

    ```
    kubectl port-forward -n central whc-7cb9799bcb-rntnx 12280:8080
    ```
    ![kubectl port-forward -n central....](../images/get-pods-whc-3.png)


#### Port forward for the MDM Kubernetes pod

Port forward instructions for the `MDM` pod in a PowerShell session.

**Steps**:

1. Use this command to list all pod names in the `central` namespace. You can then copy the pod name for MDM:

    ```
    kubectl get pods -n central

    ```

2. You can also use this command to find the specific pod name:

    ```
    kubectl get pods -n central --no-headers | Select-String '^mdm-' | Where-Object { $_.Line -notmatch '^mdm-nginx' } | ForEach-Object { $_.Line.Split()[0] }
    ```

3. Issue this command to forward the port for your deployment:

    ```
    kubectl port-forward -n central <mdm-pod-name> 12580:8080
    ```

4. This is an example of a port forward for the MDM pod:

    ```
    kubectl port-forward -n central mdm-67997bd8c8-n5mqh 12580:8080
    ```
    ![kubectl port-forward -n central mdm](../images/port-forward-mdm.png)

#### Port forward for the INB Kubernetes pod

Port forward instructions for the `INB` pod in a PowerShell session.

**Steps**:

1. Use this command to list all pod names in the `central` namespace. You can then copy the pod name for INB:

    ```
    kubectl get pods -n central

    ```

2. You can also use this command to find the specific pod name:

    ```
    kubectl get pods -n central --no-headers | Select-String '^inb' | ForEach-Object { $_.Line.Split()[0] }
    ```

3. Issue this command to forward the port for your deployment:

    ```
    kubectl port-forward -n central <inb-pod-name> 18180:8080
    ```

4. This is an example of a port forward command for the INB pod:

    ```
    kubectl port-forward -n central inb-85cb9598bf-b84ww 18180:8080
    ```
    !!! note

        As mentioned above, port forwarding of the INB pod is necessary to successfully run a DVF emulation and induct a case. This step is not necessary or part of a successful Terraform setup.


### Generating the warehouse with scripts

After successfully port forwarding the specified Kubernetes pods, you'll need to generate the warehouse as well as create and activate SKUs.

**Steps**:

1. Navigate to the `<my-git-repo-demo-dev-local>\intaketests` directory and run the `1-generate-warehouse.bat` script in a terminal session. Your output should look something like this:

    ![Generate the warehouse](../images/generate-warehouse-script.png)

2. In the same directory, run the `2-create-skus.bat` script:

    ![Create SKUs script](../images/create-skus-script.png)

3. Now run the `3-activate-skus.bat` script. There is no output for this script:

    ![Activate SKUs script](../images/activate-skus-script.png)

4. Now run the `4-create-facility-advice.bat` script. There is no output for this script:

    ![Create facility advice](../images/create-facility-advice-script.png)

You can now install and run DVF.