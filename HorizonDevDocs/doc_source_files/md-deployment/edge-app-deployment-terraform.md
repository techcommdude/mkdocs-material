!!! note

    Before starting this procedure, you must ensure that your Kubernetes cluster is running in Docker Desktop.  For more information, see [Docker configuration for a Kubernetes cluster](docker-desktop.md#docker-configuration-for-a-kubernetes-cluster).

The Terraform scripts that you run here will set up the `edge` application for a Horizon deployment. By default, this template uses the kubectl context of `docker-desktop` for connecting to the Kubernetes cluster. This context is stored in your local `~/.kube/config` file.

**Steps**:

1. Navigate to this directory in your repository:

    `<my-git-repo-demo-dev-local>\terraform\application\hzn-edge`

2. Verify that you are currently using a kube context of `docker-desktop` context with this command:

    ```
    kubectl config get-contexts

    ```

3. Update your helm repository with the `helm repo update` command:

    !!! note

        If you receive an error for the above command, you may need to ensure that helm is properly installed and configured. For more information, see [Helm installation and configuration](helm.md#helm-installation-and-configuration).

4. To initialize your local system and backend, run the `terraform init` command. Your output should look something like this:

    ![terraform init](../images/hzn-edge-terraform-init.png)

5. If you want to view the configuration changes without changing anything, run the `terraform plan` command.

6. To update the cluster with the changes for the current Terraform configuration, run the `terraform apply` command. You will have to enter **yes** to approve the plan and configuration changes:

    ![terraform apply](../images/hzn-edge-terraform-apply.png)

7. If the `terraform apply` command was successful, Terraform will create a *.terraform* directory and other Terraform-specific files within the directory where you ran the command.