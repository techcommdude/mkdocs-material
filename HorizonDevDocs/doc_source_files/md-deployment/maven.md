
If you haven't already, you will need to install Maven to build the warehouse and deploy the Horizon UI in later steps in this document.

!!! note

    Maven requires an installation of Java. As of writing, this is the current supported release: [https://github.com/corretto/corretto-18/releases](https://github.com/corretto/corretto-18/releases){:target="_blank"}

This wiki page in the Horizon space has good information on how to install and configure Maven:

* [https://wiki.dematic.com/display/IQH/Apache+Maven+configuration](https://wiki.dematic.com/x/kz88J){:target="_blank"}

You can also refer to this wiki page in the Dematic IQ Optimize space:

* [https://wiki.dematic.com/display/dcdng/Tools+installation#Toolsinstallation-mavenInstallApacheMaven](https://wiki.dematic.com/display/dcdng/Tools+installation#Toolsinstallation-mavenInstallApacheMaven){:target="_blank"}

These are the Maven environment variables you must set:

![](../images/maven-environment.png)

This is an example of the Maven repository, the location of the repository and the location of the *settings.xml* file:

![](../images/repository-maven.png)

Example of the contents of the repository:

![](../images/maven-repository-contents.png)

