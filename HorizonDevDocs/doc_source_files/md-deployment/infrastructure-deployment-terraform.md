<!--  TODO: Add more overview information here. -->

!!! warning

    This section is currently under construction.

!!! note

    Before starting this procedure, you must ensure that your Kubernetes cluster is running in Docker Desktop.  For more information, see [Docker configuration for a Kubernetes cluster](docker-desktop.md#docker-configuration-for-a-kubernetes-cluster).

The Terraform scripts that you run here will set up the `central` and `edge` infrastructure for a Horizon deployment. By default, this template uses the kubectl context of `docker-desktop` for connecting to the Kubernetes cluster. This context is stored in your local `~/.kube/config` file.

**Steps**:

1. Navigate to this directory in your repository:

    `<my-git-repo-demo-dev-local>\terraform\infrastructure\inf-devk8s-all`

2. Verify that you are currently using a kube context of `docker-desktop` context with this command:

    ```
    kubectl config get-contexts

    ```
    Your output should look something like this. If it doesn't, you will need to set the appropriate context using kubectl:

    ![kubectl get contexts command](../images/infrastructure-get-contexts.png)

3. Update your helm repository with the `helm repo update` command:

    ![helm repo update command](../images/helm-repo-update.png)

    !!! note

        If you receive an error for the above command, you may need to ensure that helm is properly installed and configured. For more information, see [Helm installation and configuration](helm.md#helm-installation-and-configuration).

4. To initialize your local system and backend, run the `terraform init` command:

    ![terraform init](../images/infrastructure-terraform-init.png)

5. If you want to view the configuration changes without changing anything, run the `terraform plan` command.

6. You can update the cluster with the changes for the current Terraform configuration by running the `terraform apply` command. You will have to enter **yes** to approve the plan and configuration changes:

    ![terraform apply command](../images/terraform-apply-1.png)
    ![terraform apply command](../images/terraform-apply-2.png)

    !!! tip

        You can also use `terraform apply -auto-approve` to avoid having to type **yes** to start the apply.

7. If the `terraform apply` command was successful, Terraform will create a *.terraform* directory and other Terraform-specific files:

    ![Terraform infrastructure files](../images/terraform-apply-infrastructure-files.png)
