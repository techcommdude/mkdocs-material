
**Steps**:

1. Navigate to the `<my-git-repo-demo-dev-local>\intaketests` directory.
2. You can induct a case into the warehouse by running the `5-induct-one-case-SKU12.bat` script in a terminal session. Your output will look something like this:

    ![induct one case script](../images/induct-case-script-dvf.png)

3. If you launch DVF at this point, you should notice that most PLC indicator lights are green:

    ![PLCs green](../images/horizon-plcs.png)
