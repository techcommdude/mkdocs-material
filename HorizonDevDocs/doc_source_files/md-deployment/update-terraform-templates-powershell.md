<!--  TODO: Add more overview information here. -->

!!! warning

    This section is currently under construction.


After forking the project, you will need to run a script to get the latest versions of the Terraform templates maintained by Dematic Development.

**Steps**:

1. Run the `update-templates.ps1` PowerShell script that's found in the `/git-repository-name>/terraform` directory of the forked project:

    ![Running the script](../images/dev-local-running-script.png)

    !!! note

        You may need to periodically run the `update-templates.ps1` script to ensure you have the latest Terraform templates from Dematic Development.


<!--TODO: Notes for myself.
* Would the user do the above to grab the latest?
* Would they also make use of override files and customizations to make their changes so they do not overwrite the provided Terraform templates? 
* 
* -->