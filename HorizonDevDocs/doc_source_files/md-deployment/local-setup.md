<!--  TODO: Add more overview information here. -->

!!! warning

    This section is currently under construction.

This section outlines how to use an existing demo project (in GitLab) to deploy Horizon on your local machine using a Kubernetes cluster in Docker Desktop. This can be useful for testing new features, demos, and as a proof-of-concept (POC) for an actual customer cloud-based deployment in Google Cloud Platform (GCP).

This section assumes you have installed and configured all software that pertains to a local Kubernetes cluster such as Docker Desktop. For more information on the required software and configuration details, see [Project software installation and configuration](./software-installation-configuration.md).