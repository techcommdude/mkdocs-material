

<!-- TODO: (Why do users need this?  Explain.) -->

!!! note

    If you need access to Artifactory, you can log a DevOps support ticket [here](https://jira.dematic.com/servicedesk/customer/portal/2){:target="_blank"}.

This topic will outline how to configure access to the Horizon Helm repositories in Artifactory.

**Steps**:

1. Using the **Set Me Up** tool in Artifactory, set up a Helm client:

    ![](../images/artifactory-helm.png)

2. On the **Configure** tab, select the **horizon-helm** repository, enter your password and click **Generate Token & Create Instructions**:

    ![](../images/artifactory-helm-4.png)

3. Artifactory displays the generated token value:

    ![](../images/artifactory-helm-2.png)

    Save the token value since you won't be able to view it again and you may need to use the Artifactory token value again in the future.

    !!! note

        The instructions for adding the repository to Helm are different from what appears in Artifactory. For details, see [Helm Installation and configuration](#helm-installation-and-configuration).


4. Click **Done**.  The identity token for the **horizon-helm** repository is now part of your Artifactory profile.

    ![](../images/artifactory-helm-3.png)
