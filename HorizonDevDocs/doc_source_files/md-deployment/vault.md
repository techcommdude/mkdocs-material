

<!--  TODO: What version? -->

Vault validates and authorizes clients (users, machines, apps) before providing them access to secrets or stored sensitive data. For more information on what Vault is, see the [Glossary](../md-glossary/glossary.md#vault-by-hashicorp).


**Steps**:

1. Download the latest version from here:

    * [https://developer.hashicorp.com/vault/downloads](https://developer.hashicorp.com/vault/downloads){:target="_blank"}

    ![](../images/vault-download.png)

2. Place the extracted binary (_vault.exe_) on your system and add it to your _PATH_ environment variable:

    ![](../images/path-vault.png)

3. Verify the installation by running the `vault` command:

    ```
    vault

    ```

    ![](../images/vault-installation-validation.png)

4. To verify the version of Vault, use the `vault --version` command:

    ![](../images/vault-version.png)

### Certificate setup for the Vault server

To access the Vault server and avoid certificate errors in your browser, you must install the Root CA on your computer. This is the same certificate that was used to sign the SSL certificate on the Vault server.

**Steps**:

1. Download the certificate to your local machine from GitLab: 

    * [https://gitlab.dematic.com/horizon/playground/nginx/-/blob/master/createcerts/DematicSPE_CA.pem](https://gitlab.dematic.com/horizon/playground/nginx/-/blob/master/createcerts/DematicSPE_CA.pem){:target="_blank"}

2. In Windows, select **Certificate Manager** in the Control Panel and then right-click the **Certificates** folder and select **All Tasks** > **Import** to import the certificate:

    ![](../images/import-certificates.png)

    Use the wizard to install the certificate to the directory selected above.

3. You should get a successful import message from the tool.

4. After successful installation of the certificate, you should be able to open this site without a certificate error:

    * [https://vault.horizondev.dev/](https://vault.horizondev.dev/){:target="_blank"}

### Vault CLI environment variables

To avoid having to pass the address of the Dematic Vault server on the command line, you can set the *VAULT_ADDR* environment variable to a value of *https://vault.horizondev.dev*:

**Steps**:

1. Set the environment variable:

    ![](../images/vault-address-env-variable.png)

2. To verify the environment variable, use `$env:VAULT_ADDR`:

    ![](../images/vault-variable.png)

### First time log in to the Horizon Vault server

You can log in to the Horizon Vault server [(https://vault.horizondev.dev)](https://vault.horizondev.dev){:target="_blank"} with your Google Cloud credentials. After logging in, you can copy the token value that enables you to log in at the Vault CLI.

**Steps**:

1. Sign in to Horizon Vault [(https://vault.horizondev.dev)](https://vault.horizondev.dev){:target="_blank"} in your browser with your Google Cloud credentials:

    ![](../images/vault-google.png)

2. You can also use the Vault CLI to log in with this command. The Vault CLI will launch your browser:

    ```
    vault login -method=oidc -path=gcp-auth

    ```

3. Give access to Vault:

    ![Alt text](../images/vault-google-access.png)

4. After logging in to Vault, use the **Copy Token** menu option to retrieve the value of your access token. You should save the value in a password manager or somewhere secure:

    ![](../images/vault-access-token.png)

    After the initial login, you no longer need to supply the token value to use Vault at the command line.

5. You can now use the `vault login` command in PowerShell to access your vault on the command line. You need to specify the copied token and press **Enter** on your keyboard:

    ![](../images/vault-login-command-line.png)

6. You can also log in to the Vault UI using your token value:

    ![](../images/vault-token-login.png)


### Renew and copy your token in Vault

With server upgrades and other changes, you may need to renew your access token.

**Steps**:

1. Sign in to Horizon Vault [(https://vault.horizondev.dev)](https://vault.horizondev.dev){:target="_blank"} in your browser with your Google Cloud credentials:

    ![](../images/vault-google.png)

2. In your profile settings, select **Renew token** followed by **Copy token**:

    ![vault renew token](../images/renew-vault-token.png)

    Make sure to record and store the token value somewhere secure as you cannot access the value again.

3. Now you can specify the token value when you log in to Vault CLI:

    ![vault renew token](../images/vault-login-command-line.png)


### Log out of Vault CLI

Vault doesn't provide a way to "logout" via the CLI, however the behavior can be replicated by deleting the "helper token". The Vault CLI uses this "token helper" to cache the token after authentication. This is conceptually similar to how a website securely stores your session information as a cookie in the browser.

#### Windows

Run this command in PowerShell:

```
Remove-Item ~/.vault-token

```

#### MacOS and Linux

Run this command:

```
rm ~/.vault-token

```

