# API documentation

You can find API documentation samples in this section.

## Swagger API docs

You can generate and display Swagger API documentation in GitLab at no cost.

!!! tip

    This would eliminate the need to purchase a [plugin](https://marketplace.atlassian.com/apps/1218914/open-api-swagger-editor-for-confluence?tab=overview&amp%3Bhosting=cloud&hosting=cloud){:target="_blank"}
    to display this API documentation in Confluence.

Here are some examples:

- [Swagger TAS API docs](http://diqhelp.ops.cld:8101/horizon-dev-docs/api-doc-swagger-docs/tas){:target="_blank"}
- [Swagger ORD API docs](http://diqhelp.ops.cld:8101/horizon-dev-docs/api-doc-swagger-docs/ord){:target="_blank"}

## Redocly API docs

Redocly is an open-source OpenAPI documentation tool. There is also a paid version.

Redocly API documentation can be generated and displayed in GitLab at no cost.

!!! note

    Confluence **DOES NOT** support the display of Redocly API documentation.

Some benefits of Redocly:

* Search capabilities that aren't available in Swagger OpenAPI docs.
* Superior look/feel and user experience.
* [CLI tools to validate and lint your OpenAPI definition against a rule set](https://redocly.com/docs/cli/api-standards/#api-standards-and-governance){:target="_blank"} to ensure you’re following best practices.
* Ability to split the OpenAPI definition into smaller files to make it easier to work with.
* Include Markdown content for conceptual information in your API documentation. See examples such as this [topic](http://diqhelp.ops.cld:8101/horizon-dev-docs/Redocly/mdm-redoc-static.html#section/About-this-API){:target="_blank"} that was written with Markdown in GitLab.

Here is an example:

- [Redocly MDM API docs](http://diqhelp.ops.cld:8101/horizon-dev-docs/api-doc-redocly/mdm-redoc-static.html){:target="_blank"}

<!--  TODO: Add a Redocly TAS API and ORD API document to this list, get rid of MDM. ) -->


## Doxygen Java API documentation

You can generate and display Doxygen API documentation using GitLab.

Click [here](http://diqhelp.ops.cld:8101/horizon-dev-docs/api-doc-doxygen/sample/index.html){:target="_blank"} to view
the example Doxygen Java API documentation.

