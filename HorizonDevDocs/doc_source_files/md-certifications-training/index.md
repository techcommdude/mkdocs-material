Depending on your skill-level, you should also consider certifications as a way to broaden your knowledge of Project Horizon.

Dematic certifications enable learners to level up their expertise on standard subsystems and solutions, and foundational software, controls, and mechanical engineering skills at the product level.

<!--  TODO: Add more info?) -->

## Software certifications for Project Horizon

Technical Dematic Global Software certifications typically include at least one practical exam, as well as one or more knowledge-based exams. You must meet the minimum passing score on all exams to complete a full certification but can earn credentials on knowledge exams that are prerequisites for other certifications.

More information on Project Horizon certifications can be found here:

* [https://kionnam.sharepoint.com/sites/scs_DCP/SitePages/Global-Software-Certifications.aspx#project-horizon-certifications](https://kionnam.sharepoint.com/sites/scs_DCP/SitePages/Global-Software-Certifications.aspx#project-horizon-certifications){:target="_blank"}

The SCS (Supply Chain Solutions) Application Solution Center (ASC) also contains information on training opportunities:

* [Certifications for Project Horizon](https://kionnam.sharepoint.com/sites/scs_ASC/SitePages/Dematic-FlexWES.aspx#training){:target="_blank"}