# Markdown syntax examples

The documentation on this site uses the lightweight markup language called *Markdown*, specifically this 
"flavor" of Markdown:

* [https://python-markdown.github.io/](https://python-markdown.github.io/){:target="_blank"}

!!! warning

    There is no defined standard for Markdown, therefore some of the Markdown syntax used to create this site may not
    display properly when viewed in Gitlab or IntelliJ.  However, once the site is generated the display will be correct
    as long as you use the Markdown syntax outlined in this section.

## Headings

Let's run through the basics of Markdown. For example, a single hash (`#`) denotes the title of a document, like so:

```markdown
# Heading 1
```

Specify the next level of sub-heading with two hashes, like this:

```markdown
## Heading 2
```

Each next level heading gets successively smaller, for example:

```markdown

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

```

Which becomes:

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

## Bold and italics

- To create *italic* text, it's `*one asterisk on either side of the text*`
- **Bold** text uses `**two asterisks**`
- ***Bold italic*** is `***three asterisks***`

## Links to websites

[Hyperlinks](https://www.dematic.com/){:target="_blank"} to external websites (in a new browser tab) use square brackets for the display text and
parenthesis for the hyperlink URL use this syntax:

* `[text-to-display](link/url){:target="_blank"}`

If you want to open the site in the same tab, you can exclude the `{:target="_blank"}` syntax. This is usually a matter of personal preference.

For example, `[GitLab website](https://about.gitlab.com/){:target="_blank"}` results in this: [GitLab website](https://about.gitlab.com/){:target="_blank"}.

## Cross-references/links to content within a page/file

Cross-references to named anchors/permalinks within a file/help topic use this syntax: `[Python code sample](#python-code-samples-with-highlighted-code)`, 
which results in this: [Python code sample](#python-py).

## Cross-references/links to content in another file

Cross-references to other files within the help system use this syntax: `[See Project Roles for more information](../md-getting-started/roles.md)`, which
results in this: [See Project Roles for more information](../md-getting-started/roles.md).

The above example cross-references a file that's one-level up and in a folder named *getting-started*.

You can even link to subsections within another file with this syntax that uses anchors/permalinks:
`[Software support engineer](./md-getting-started/GettingStartedIndex.md#cs-software-support-engineer)`, which results
in this: [Software support engineer](../md-getting-started/roles.md#cs-software-support-engineer).


## Lists—ordered and unordered

Unordered lists use either `*` or `-` on separate lines. Use an empty line between the introductory sentence
and your list:

```markdown
This is my intro:

* first item
* second item
* third item
```

or

```markdown
This is my intro:

- first item
- second item
- third item
```

Which becomes:

This is my intro:

- first item
- second item
- third item

Ordered lists can be created by writing successive numbers on successive lines. Use an empty line between the introductory sentence
and your list:

```markdown
This is my intro:

1. first item
2. second item
3. third item
```

Which becomes:

This is my intro:

1. first item
2. second item
3. third item

Both types of list can be sub-setted as follows:

```markdown
This is my intro:

- first item
  - sub-item
    - sub-sub-item
- second item
```

Which becomes:

This is my intro:

- first item
    - sub-item
        - sub-sub-item
- second item

## Continue numbering in ordered lists

Often you will have content between the numbered items in a list, and you want to continue numbering. To continue the
numbering, you must indent the content four spaces.

### Basic examples

```markdown
This is my intro:

1. item 1
2. item 2

    ```
    Code block between numbered list items. Indent by four spaces.
    ```
   
3. item 3

```

Which becomes:

This is my intro:

1. item 1
2. item 2

    ```
    Code block between numbered list items. Indent by four spaces.
    ```

3. item 3

### More complicated example

```markdown

1. Step 1.

    Some text between numbers. Use four spaces.

2. Step 2.

    Some more text with four preceding spaces.

3. Step 3.
4. Fourth step.

    ![](../images/Gitlab_icon.png)

    Indent this **line** by four spaces. Image above needs four spaces as well.

    Indent this _line_ by four spaces.

5. Fifth step.

```

Which becomes:

1. Step 1.

    Some text between numbers. Use four spaces.

2. Step 2.

    Some more text with four preceding spaces.

3. Step 3.
4. Fourth step.

    ![](../images/Gitlab_icon.png)

    Indent this **line** by four spaces. Image above needs four spaces as well.

    Indent this _line_ by four spaces.

5. Fifth step.

## Task lists

Markdown syntax for a task list:

```markdown
- [x] Lorem ipsum dolor sit amet, consectetur adipiscing elit
- [ ] Vestibulum convallis sit amet nisi a tincidunt
    * [x] In hac habitasse platea dictumst
    * [x] In scelerisque nibh non dolor mollis congue sed et metus
    * [ ] Praesent sed risus massa
- [ ] Aenean pretium efficitur erat, donec pharetra, ligula non scelerisque
```
Which becomes:

- [x] Lorem ipsum dolor sit amet, consectetur adipiscing elit
- [ ] Vestibulum convallis sit amet nisi a tincidunt
    * [x] In hac habitasse platea dictumst
    * [x] In scelerisque nibh non dolor mollis congue sed et metus
    * [ ] Praesent sed risus massa
- [ ] Aenean pretium efficitur erat, donec pharetra, ligula non scelerisque


## Definition lists

After enabling Definition Lists, you can enumerate lists of arbitrary key-value pairs, for example, the parameters of functions or modules, with Markdown syntax:

````markdown

`Lorem ipsum dolor sit amet`

:   Sed sagittis eleifend rutrum. Donec vitae suscipit est. Nullam tempus
tellus non sem sollicitudin, quis rutrum leo facilisis.

`Cras arcu libero`

:   Aliquam metus eros, pretium sed nulla venenatis, faucibus auctor ex. Proin
ut eros sed sapien ullamcorper consequat. Nunc ligula ante.

    Duis mollis est eget nibh volutpat, fermentum aliquet dui mollis.
    Nam vulputate tincidunt fringilla.
    Nullam dignissim ultrices urna non auctor.

````

Which becomes:

`Lorem ipsum dolor sit amet`

:   Sed sagittis eleifend rutrum. Donec vitae suscipit est. Nullam tempus
tellus non sem sollicitudin, quis rutrum leo facilisis.

`Cras arcu libero`

:   Aliquam metus eros, pretium sed nulla venenatis, faucibus auctor ex. Proin
ut eros sed sapien ullamcorper consequat. Nunc ligula ante.

    Duis mollis est eget nibh volutpat, fermentum aliquet dui mollis.
    Nam vulputate tincidunt fringilla.
    Nullam dignissim ultrices urna non auctor.

Another example:

```markdown

## My Heading


:   This is my first paragraph. This is my first paragraph. This is my first paragraph.
This is my first paragraph. This is my first paragraph. This is my first paragraph. This is my first paragraph.
This is my first paragraph. This is my first paragraph. This is my first paragraph.

:   This is my second paragraph. This is my second paragraph. This is my second paragraph. This is my second paragraph.
This is my second paragraph. This is my second paragraph. This is my second paragraph. This is my second paragraph.
This is my second paragraph. This is my second paragraph.

```

Which becomes:

## My Heading


:   This is my first paragraph. This is my first paragraph. This is my first paragraph.
This is my first paragraph. This is my first paragraph. This is my first paragraph. This is my first paragraph.
This is my first paragraph. This is my first paragraph. This is my first paragraph.

:   This is my second paragraph. This is my second paragraph. This is my second paragraph. This is my second paragraph.
This is my second paragraph. This is my second paragraph. This is my second paragraph. This is my second paragraph.
This is my second paragraph. This is my second paragraph.

## Images and screen captures

To insert images, use the structure `![alt-text](url or filepath)`, for example

```markdown
![Logo of Python4DS](https://github.com/aeturrell/python4DS/blob/main/logo.png?raw=true){ loading=lazy }
```

Which becomes:

![Logo of Python4DS](https://github.com/aeturrell/python4DS/blob/main/logo.png?raw=true){ loading=lazy }


This is an image stored locally in the *images* directory with lazy loading:

```markdown
![Dematic homepage](../images/Dematic.png){ loading=lazy }
```

Which becomes:

![Dematic homepage](../images/Dematic.png){ loading=lazy }

## Animated GIFs and PNGs

This is a *\*.gif* file stored in the *images* directory:
```markdown
![MFC Overview](../images/MFCOverview.gif){ loading=lazy }
```

Which becomes:

![MFC Overview](../images/MFCOverview.gif){ loading=lazy }

Animated PNG (*.apng*) example:

```markdown
![Animated PNG example](../images/Harden-Windows-Security-Demo.apng){ loading=lazy }
```

Which becomes:

![Animated PNG example](../images/Harden-Windows-Security-Demo.apng){ loading=lazy }

## Videos from YouTube and local videos (*.mp4*)

You can copy the embed code from YouTube videos and adjust the height and width appropriately.

The following syntax is HTML 5 code that can be obtained from the embed code for the video on YouTube:

```markdown
<iframe width="672" height="378" src="https://www.youtube.com/embed/1qJcb0HqHpU?autoplay=0" title="Reduce Warehouse Noise at the Source with Dematic Noise Reduction" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
```

Which becomes:

<iframe width="672" height="378" src="https://www.youtube.com/embed/1qJcb0HqHpU?autoplay=0" 
title="Reduce Warehouse Noise at the Source with Dematic Noise Reduction" frameborder="0" allow="accelerometer; 
autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

You can also display local *.mp4* files with the video tag from HTML 5 syntax. For tips on where to store the video so that it 
displays, you can view the file structure for this project.  You can find the video displayed below in the 
*Other/markdown-sample* directory in the local file system:

<!---
The video below will always show a "cannot resolve" error, but it works when the site is deployed.
-->

```markdown
<video width="672" height="378" controls>
    <source src="2023-07-05_MFC_only_transports.mp4" type="video/mp4">
</video>
```

Which becomes:

<!---
The video below will always show a "cannot resolve" error, but it works when the site is deployed.
-->

<video width="672" height="378" controls>
    <source src="2023-07-05_MFC_only_transports.mp4" type="video/mp4">
</video>

## Attachments and downloadable files

You can attach downloadable files to Markdown pages with this syntax. Note that you can place downloadable files in the `static` directory of your repository:

```markdown

* [Download a PDF document here!](../static/API-Documentation-Process-Guide-29-Nov-2023.pdf){:download="API-Documentation-Process-Guide-29-Nov-2023.pdf"}

```

Which becomes:

* [Download a PDF document here!](../static/API-Documentation-Process-Guide-29-Nov-2023.pdf){:download="API-Documentation-Process-Guide-29-Nov-2023.pdf"}

## Line breaks

To insert a line-break use:

```markdown
***
```

Which becomes:

***


## Tables (sortable)

Tables can be difficult to use in Markdown. In general, it's easy to create simple tables with minimal text formatting in table cells, but more complicated text formatting such as bulleted lists, numbered lists and separate paragraphs with line breaks can be more challenging. However, there are ways around these limitations that are outlined in the topics below.

The basic syntax to create tables that are sortable by clicking on the column headings is:

```markdown
First Header  | Second Header
------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell
```

Which becomes:

First Header  | Second Header
------------- | -------------
ABC123        | 111ABC
DEF123        | 222dDEF

Another example:
```markdown
| Tables   |      Are      |  Cool |
|----------|:-------------:|------:|
| col 1 is |  left-aligned | $1600 |
| col 2 is |    centered   |   $12 |
| col 3 is | right-aligned |    $1 |
```
Which becomes:

| Tables   |      Are      |  Cool |
|----------|:-------------:|------:|
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |   $12 |
| col 3 is | right-aligned |    $1 |

!!! note

    Sticky table headers are not currently supported for this site.

### Markdown table generators—Example 1

Due to the finicky syntax of tables in Markdown, it's often easiest to use one of many handy websites to generate the Markdown table syntax for you. This site is useful for formatting Markdown tables: 

* [https://stevecat.net/table-magic/#](https://stevecat.net/table-magic/#){:target="_blank"}

Steps:

1. You can create tables with the **Form** tab by clicking **New table** and specifying the number of rows and columns for your table:

    ![Table magic Form tab](../images/table-magic-form-tab.png)

    You can also add and delete rows and columns on this tab.

2. Add content to your table with the **Markdown** tab:

    ![Table magic Markdown tab](../images/table-magic-markdown-tab.png)

3. Preview your content with the **Preview** tab:

    ![Table magic Preview tab](../images/table-magic-preview-tab.png)

4. If you need to add rows or columns it is often easier to copy the table in Markdown and then paste it in the Markdown tab of the [site](https://stevecat.net/table-magic/#){:target="_blank"}.

5. You can then use the **Form** tab to add and delete columns as rows and even modify your content.

    ![Adding rows and columns](../images/table-magic-form-tab-add-row-column.png)

6. After modifying the table, you can copy the generated content in the **Markdown** tab (see screen capture above) and paste it into your documentation.

Here's a video on YouTube with more information on this tool: [Table Magic on YouTube](https://www.youtube.com/watch?v=2qUx0Ignn38&ab_channel=GitHub){:target="_blank"}


### Markdown table generators—Example 2

Here's another example of a site that helps you create tables with Markdown:

* [Markdown table generator](https://www.tablesgenerator.com/markdown_tables){:target="_blank"}

**Steps**:

1. Navigate to the site mentioned above and select **Table** > **Set Size** and click the number of rows and columns you want for your table.
    
    ![Set the size of the table](../images/table-generator-set-size.png)

2. Click **Generate** to generate the syntax of your table and then click **Copy to clipboard**. You can now paste the Markdown table syntax into your Markdown source file and fill in the content.

    ![Copy the Markdown table syntax](../images/table-generator-syntax.png)

!!! tip

    You can also add columns and rows to the table in your Markdown content if your table requirements change. The above tool allows you to get started with the correct syntax and little effort.

### Line breaks, ordered lists, unordered lists in table cells

Tables are notoriously finicky in Markdown. For instance, you can't use block-level formatting with line breaks. However, there are a few workarounds to make them more usable:

* Force a line break in a table cell with the `<br>` HTML tag.
* Create an ordered list in a table cell with this HTML syntax: `<ol><li>First</li><li>Second</li></ol>`
* Create an unordered list in a table cell with this HTML syntax: `<ul><li>First</li><li>Second</li></ul>`
* Bold, italics, inline code syntax and other inline formatting work as well.

Examples of the above syntax and workarounds are found in the code sample below and in the resulting table that follows. Although the syntax looks complicated, it is actually just a fully fleshed out version of this syntax that was generated using the [table generator](https://www.tablesgenerator.com/markdown_tables){:target="_blank"} mentioned above:

```markdown

|  	|  	|  	|  	|  	|
|---	|---	|---	|---	|---	|
|  	|  	|  	|  	|  	|
|  	|  	|  	|  	|  	|
|  	|  	|  	|  	|  	|

```


If you have issues with formatting tables, contact a member of the technical documentation team for help:

```markdown
|Name                                   |Default            |Description      |Another column     |Yet another column|   
|--- |--- |--- |--- |---|
|Scoring Strategy                       |Weighted Sum       |Scoring strategy is applied to all scoring destination selectors (like `AISLE` selector).<br>  Configurable values are:<br> <ol><li>**Weighted sum**: uses a weighted sum of the preferences' scores to consider when selecting a destination. (default)</li><li>**Order by**: provide the old-fashioned behavior of destination selection with a simple database query *order by* sortation of the results.</li></ol>|Another column|   Yet another column|
|Aisle Max Fill Level                   |0.98               |<ul><li>The `Max Fill Level` is a value between `0` and `1` that indicates how full an aisle can get before it will not be selected anymore as a destination.</li><li>Lowering this value will mean that less storage is possible in general, but the storage will be better optimized because of sufficient space for ideal bin selections.</li></ul>|Another column|   Yet another column|
|Aisle Fill Level Imbalance Threshold   |0.95               |The imbalance threshold for groups is a value between `0` and `1` and determines when an aisle is considered imbalanced and shall be preferably chosen during aisle selection.|Another column  |   Yet another column|
|Allow Destination Selection When Equipment Unavailable |true   |Per default aisles and levels that are technically unavailable will be included in the destination selection process because these locks are short term locks.<br><br> Setting this parameter to false will mean that such aisles and levels are not considered during destination selection.|Another column|   Yet another column|
|Prefer Partial Threshold               |0                  |This parameter is specific to the Empty-Partial strategy. When the number of empty locations in an aisle or level is greater than this threshold, empty locations will be preferred over partially filled locations.|Another column|   Yet another column|
|LHD Clearance Limit                    |32                 |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective x extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |41                 |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |39                 |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |2                  |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |0                  |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |25                 |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |39                 |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |1                  |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |5                  |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |4                  |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |9                  |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |6                  |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Another column|
|Yet another row!                       |123                |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |3876               |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|

```

Which becomes:

|Name                                   |Default            |Description      |Another column     |Yet another column|   
|--- |--- |--- |--- |---|
|Scoring Strategy                       |Weighted Sum       |Scoring strategy is applied to all scoring destination selectors (like `AISLE` selector).<br>  Configurable values are:<br> <ol><li>**Weighted sum**: uses a weighted sum of the preferences' scores to consider when selecting a destination. (default)</li><li>**Order by**: provide the old-fashioned behavior of destination selection with a simple database query *order by* sortation of the results.</li></ol>|Another column|   Yet another column|
|Aisle Max Fill Level                   |0.98               |<ul><li>The `Max Fill Level` is a value between `0` and `1` that indicates how full an aisle can get before it will not be selected anymore as a destination.</li><li>Lowering this value will mean that less storage is possible in general, but the storage will be better optimized because of sufficient space for ideal bin selections.</li></ul>|Another column|   Yet another column|
|Aisle Fill Level Imbalance Threshold   |0.95               |The imbalance threshold for groups is a value between `0` and `1` and determines when an aisle is considered imbalanced and shall be preferably chosen during aisle selection.|Another column  |   Yet another column|
|Allow Destination Selection When Equipment Unavailable |true   |Per default aisles and levels that are technically unavailable will be included in the destination selection process because these locks are short term locks.<br><br> Setting this parameter to false will mean that such aisles and levels are not considered during destination selection.|Another column|   Yet another column|
|Prefer Partial Threshold               |0                  |This parameter is specific to the Empty-Partial strategy. When the number of empty locations in an aisle or level is greater than this threshold, empty locations will be preferred over partially filled locations.|Another column|   Yet another column|
|LHD Clearance Limit                    |32                 |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective x extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |41                 |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |39                 |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |2                  |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |0                  |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |25                 |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |39                 |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |1                  |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |5                  |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |4                  |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |9                  |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |6                  |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Another column|
|Yet another row!                       |123                |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|
|Yet another row!                       |3876               |This parameter is used for the *BinFlexLhdRestriction* which is only applied for flexible LHDs.<br><br> It defines how much smaller a handling units x extension can be than the effective ***x*** extension of a Handling Unit stored behind it to ensure retrieval or storage with a flex LHD without obstacles.|Another column|   Yet another column|


## Admonitions such as notes, warnings, danger, etc

This is the Markdown syntax. For your convenience, you can copy the syntax with the code-copy button:

```markdown
!!! note

    Each of these lines requires 4 preceding spaces. consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

```markdown
!!! warning

    Each of these lines requires 4 preceding spaces. consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

```markdown
!!! tip

    Each of these lines requires 4 preceding spaces. consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

```

```markdown
???+ note "Note with open styled details"

    ??? danger "Nested details!"
        This line has 8 spaces. And more content again.

```

```markdown
???- danger "Danger with closed styled details"

    ??? warning "Nested details!"
        And more content again. 8 spaces!

```

Which becomes:

!!! note

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! warning

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! tip

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

???+ note "Note with open styled details"

    ??? danger "Nested details!"
        And more content again.

???- danger "Danger with closed styled details"

    ??? warning "Nested details!"
        And more content again.


## Code blocks and syntax highlighting

Features supported:

 * Syntax highlighting for hundreds of different languages.
 * Highlights for specific lines of code and line ranges.
 * Code copy button. Confluence requires you to double-click the code block, then copy.

??? info inline end "Confluence support"

    Highlighting specific lines of code is not supported in [Confluence](https://jira.atlassian.com/browse/CONFSERVER-37494){:target="_blank"}.

 * Line numbering.
 * Displaying the file name.
 * Hundreds of languages are supported. See the following link for more 
information: [https://pygments.org/languages/](https://pygments.org/languages/){:target="_blank"}

This section contains just a few examples. Feel free to add more useful syntax examples.

### Python (*.py)

This is Markdown syntax for a Python code sample with file name, line numbering and code-copy button.

````markdown

``` python linenums="1" title="pythonFile.py" hl_lines="2"
import pandas as pd
df = pd.DataFrame([[1, 2, 3], [4, 5, 6], [7, 8, 9]]),
                columns=['a', 'b', 'c'])
```


````

Which becomes:

``` python linenums="1" title="pythonFile.py" hl_lines="2"
import pandas as pd
df = pd.DataFrame([[1, 2, 3], [4, 5, 6], [7, 8, 9]]),
                  columns=['a', 'b', 'c'])
```

Another python example:

````markdown

``` python linenums="1" title="smallFile.py"
import tensorflow as tf
    ```

````

Which becomes:

``` python linenums="1" title="smallFile.py"
import tensorflow as tf
```

### Java (*.java)

Code sample with the name of the file, line numbering, code-copy button, and highlighting of multiple line ranges.

````markdown

``` java title="myJavaFile.java" linenums="1" hl_lines="8-20 28-32"
import java.awt.Rectangle;

public class ObjectVarsAsParameters
{	public static void main(String[] args)
	{	go();
	}
	
	public static void go()
	{	Rectangle r1 = new Rectangle(0,0,5,5);
		System.out.println("In method go. r1 " + r1 + "\n");
		// could have been 
		//System.out.prinltn("r1" + r1.toString());
		r1.setSize(10, 15);
		System.out.println("In method go. r1 " + r1 + "\n");
		alterPointee(r1);
		System.out.println("In method go. r1 " + r1 + "\n");
		
		alterPointer(r1);
		System.out.println("In method go. r1 " + r1 + "\n");
	}
	
	public static void alterPointee(Rectangle r)
	{	System.out.println("In method alterPointee. r " + r + "\n");
		r.setSize(20, 30);
		System.out.println("In method alterPointee. r " + r + "\n");
	}
	
	public static void alterPointer(Rectangle r)
	{	System.out.println("In method alterPointer. r " + r + "\n");
		r = new Rectangle(5, 10, 30, 35);
		System.out.println("In method alterPointer. r " + r + "\n");
	}
	
	
}
```
````

Which becomes:

``` java title="myJavaFile.java" linenums="1" hl_lines="8-20 28-32"
import java.awt.Rectangle;

public class ObjectVarsAsParameters
{	public static void main(String[] args)
	{	go();
	}
	
	public static void go()
	{	Rectangle r1 = new Rectangle(0,0,5,5);
		System.out.println("In method go. r1 " + r1 + "\n");
		// could have been 
		//System.out.prinltn("r1" + r1.toString());
		r1.setSize(10, 15);
		System.out.println("In method go. r1 " + r1 + "\n");
		alterPointee(r1);
		System.out.println("In method go. r1 " + r1 + "\n");
		
		alterPointer(r1);
		System.out.println("In method go. r1 " + r1 + "\n");
	}
	
	public static void alterPointee(Rectangle r)
	{	System.out.println("In method alterPointee. r " + r + "\n");
		r.setSize(20, 30);
		System.out.println("In method alterPointee. r " + r + "\n");
	}
	
	public static void alterPointer(Rectangle r)
	{	System.out.println("In method alterPointer. r " + r + "\n");
		r = new Rectangle(5, 10, 30, 35);
		System.out.println("In method alterPointer. r " + r + "\n");
	}
	
	
}
```

### Terraform configuration files (*.hcl)

````markdown
```hcl linenums="1" title="terraFile.hcl" hl_lines="4-13 23"
resource "kubernetes_service" "nats" {
  depends_on = [helm_release.nats]

  metadata {
    name      = "nats-nlb"
    namespace = local.central_namespace
    annotations = {
      "networking.gke.io/load-balancer-type" = "External"
    }
    labels = {
      app = "nats"
    }
  }

  spec {
    port {
      name     = "nats"
      port     = "4222"
      protocol = "TCP"
    }

    selector = {
      "app.kubernetes.io/name" : "nats"
    }

    type = "LoadBalancer"
    load_balancer_ip = google_compute_address.nats_ip_address.address

    # Uncomment this and set a list of source cidr ranges to allow access to this external ingress. Leaving this
    # commented out will result in the default allowed range being 0.0.0.0/0
    # load_balancer_source_ranges = ["0.0.0.0/0"]
  }
}
```
````

Which becomes:

```hcl linenums="1" title="terraFile.hcl" hl_lines="4-13 23"
resource "kubernetes_service" "nats" {
  depends_on = [helm_release.nats]

  metadata {
    name      = "nats-nlb"
    namespace = local.central_namespace
    annotations = {
      "networking.gke.io/load-balancer-type" = "External"
    }
    labels = {
      app = "nats"
    }
  }

  spec {
    port {
      name     = "nats"
      port     = "4222"
      protocol = "TCP"
    }

    selector = {
      "app.kubernetes.io/name" : "nats"
    }

    type = "LoadBalancer"
    load_balancer_ip = google_compute_address.nats_ip_address.address

    # Uncomment this and set a list of source cidr ranges to allow access to this external ingress. Leaving this
    # commented out will result in the default allowed range being 0.0.0.0/0
    # load_balancer_source_ranges = ["0.0.0.0/0"]
  }
}
```

### Terraform (*.tf)

This is Markdown syntax for a Terraform file:

````markdown

``` terraform linenums="1" title="variables.tf" hl_lines="9 15 21 27"
/*
Dematic - Boilerplate
---variables.tf---
*/

variable "project_base_name" {
  type        = string
  description = "The base string that will be used to construct the host project ID."
  #  default = "<REPLACE ME>"
}

variable "folder_id" {
  type        = string
  description = "The folder ID where the project will reside."
  #  default = "<REPLACE ME>"
}

variable "billing_account" {
  type        = string
  description = "The billing account number used for the project."
  #  default = "<REPLACE ME>"
}

variable "terraform_service_account" {
  type        = string
  description = "The service account to attempt to run as. The user running this must have the ability to get an access token for that Service Account."
  #  default = "<REPLACE ME>"
}

variable labels {
  type = map(string)
  #  default = {
  #    application  = ""
  #    contact      = ""
  #    costcenter   = ""
  #    createdby    = ""
  #    createdon    = ""
  #    customer     = ""
  #    environment  = ""
  #    lifetime     = ""
  #    owner        = ""
  #  }
}

variable "skip_project_delete" {
  type        = bool
  default     = false
  description = "If set to true, created project will not be destroyed via terraform."
}

```

````

Which becomes:

``` terraform linenums="1" title="variables.tf" hl_lines="9 15 21 27"
/*
Dematic - Boilerplate
---variables.tf---
*/

variable "project_base_name" {
  type        = string
  description = "The base string that will be used to construct the host project ID."
  #  default = "<REPLACE ME>"
}

variable "folder_id" {
  type        = string
  description = "The folder ID where the project will reside."
  #  default = "<REPLACE ME>"
}

variable "billing_account" {
  type        = string
  description = "The billing account number used for the project."
  #  default = "<REPLACE ME>"
}

variable "terraform_service_account" {
  type        = string
  description = "The service account to attempt to run as. The user running this must have the ability to get an access token for that Service Account."
  #  default = "<REPLACE ME>"
}

variable labels {
  type = map(string)
  #  default = {
  #    application  = ""
  #    contact      = ""
  #    costcenter   = ""
  #    createdby    = ""
  #    createdon    = ""
  #    customer     = ""
  #    environment  = ""
  #    lifetime     = ""
  #    owner        = ""
  #  }
}

variable "skip_project_delete" {
  type        = bool
  default     = false
  description = "If set to true, created project will not be destroyed via terraform."
}

```

### JSON (*.json)

JSON Markdown syntax example:

````markdown

```json title="employees.json" linenums="1" hl_lines="2-4"
{"employees":[  
    {"name":"Shyam", "email":"shyamjaiswal@gmail.com"},  
    {"name":"Bob", "email":"bob32@gmail.com"},  
    {"name":"Jai", "email":"jai87@gmail.com"}  
]} 

```
````

Which becomes:

```json title="employees.json" linenums="1" hl_lines="2-4"
{"employees":[
  {"name":"Shyam", "email":"shyamjaiswal@gmail.com"},
  {"name":"Bob", "email":"bob32@gmail.com"},
  {"name":"Jai", "email":"jai87@gmail.com"}
]}

```

### Docker

````markdown

```dockerfile linenums="1" hl_lines="1"
docker compose up -d
docker ps
```

````

Which becomes:

```dockerfile linenums="1" hl_lines="1"
docker compose up -d
docker ps
```

### HTML (*.html)

Markdown syntax example for HTML. Syntax highlighting is shown on lines 4-9, 12, 16, 20, 24:

````markdown
```html linenums="1" hl_lines="4-9 12 16 20 24"
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Advanced Search</title>
    <link rel="stylesheet" href="search.css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" 
          rel="stylesheet" />
</head>

<body>
<div align="left">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
</div>
<div align="right">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
    eiusmod tempor incididunt ut labore et dolore magna aliqua.
</div>
<div align="center">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
    o eiusmod tempor incididunt ut labore et dolore magna aliqua.
</div>
<div align="justify">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
    ed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
</div>
</body>
</html>
```
````

Which becomes:

```html linenums="1" hl_lines="4-9 12 16 20 24"
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Advanced Search</title>
    <link rel="stylesheet" href="search.css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" 
          rel="stylesheet" />
</head>

<body>
<div align="left">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
</div>
<div align="right">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
    eiusmod tempor incididunt ut labore et dolore magna aliqua.
</div>
<div align="center">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
    o eiusmod tempor incididunt ut labore et dolore magna aliqua.
</div>
<div align="justify">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
    ed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
</div>
</body>
</html>
```

Note that there is syntax highlighting of data types and reserved keywords. The syntax highlighting supports 
a wide range of languages.

### Bash Shell

````markdown

```shell linenums="1" hl_lines="4-16"
###############################################################################
# Disclaimer print
###############################################################################
function warn_certs_not_for_production()
{
    if [ "$FORCE_NO_PROD_WARNING" != "true" ]; then
        tput smso
        tput setaf 3
        echo "Certs generated by this script are not for production (e.g. they have hard-coded passwords of '${ROOT_CA_PASSWORD}'."
        echo "This script is only to help you understand Azure IoT Hub CA Certificates."
        echo "Use your official, secure mechanisms for this cert generation."
        echo "Also note that these certs will expire in ${DEFAULT_VALIDITY_DAYS} days."
        tput sgr0
    fi
    exit 0
}
 
###############################################################################
#  Checks for all pre reqs before executing this script
###############################################################################
function check_prerequisites()
{
    local exists=$(command -v -- openssl)
    if [ -z "$exists" ]; then
        echo "openssl is required to run this script, please install this before proceeding"
        exit 1
    fi
 
    if [ ! -f ${OPENSSL_CONFIG_FILE} ]; then
        echo "Missing configuration file ${OPENSSL_CONFIG_FILE}"
        exit 1
    fi
}

```

````

Which becomes:

```shell linenums="1" hl_lines="4-16"
###############################################################################
# Disclaimer print
###############################################################################
function warn_certs_not_for_production()
{
    if [ "$FORCE_NO_PROD_WARNING" != "true" ]; then
        tput smso
        tput setaf 3
        echo "Certs generated by this script are not for production (e.g. they have hard-coded passwords of '${ROOT_CA_PASSWORD}'."
        echo "This script is only to help you understand Azure IoT Hub CA Certificates."
        echo "Use your official, secure mechanisms for this cert generation."
        echo "Also note that these certs will expire in ${DEFAULT_VALIDITY_DAYS} days."
        tput sgr0
    fi
    exit 0
}
 
###############################################################################
#  Checks for all pre reqs before executing this script
###############################################################################
function check_prerequisites()
{
    local exists=$(command -v -- openssl)
    if [ -z "$exists" ]; then
        echo "openssl is required to run this script, please install this before proceeding"
        exit 1
    fi
 
    if [ ! -f ${OPENSSL_CONFIG_FILE} ]; then
        echo "Missing configuration file ${OPENSSL_CONFIG_FILE}"
        exit 1
    fi
}

```

## Inline code highlighting

Inline code is shown with backticks, like this ``` `my inline code` ```.  For example, `This is my inline
code sample`.

Use a shebang (#!) to create inline code highlighting with the language specified:

The ``` `#!python range()` ``` function is used to generate a sequence of numbers.

Which becomes:

The `#!python range()` function is used to generate a sequence of numbers.

Another example:

Specify this HTML in the file: ``` `#!html <link rel="stylesheet" href="search.css" />`  ```

Which becomes:

Specify this HTML in the file: `#!html <link rel="stylesheet" href="search.css" />`

## Content tabs

You can group alternative content (code blocks or other content) under different tabs, e.g. when describing how to access an 
API from different languages or environments. 

Markdown syntax:

````markdown

=== "C"

    ``` c
    #include <stdio.h>

    int main(void) {
      printf("Hello world!\n");
      return 0;
    }
    ```

=== "C++"

    ``` c++
    #include <iostream>

    int main(void) {
      std::cout << "Hello world!" << std::endl;
      return 0;
    }
    ```


````


Which becomes:

=== "C"

    ``` c
    #include <stdio.h>

    int main(void) {
      printf("Hello world!\n");
      return 0;
    }
    ```

=== "C++"

    ``` c++
    #include <iostream>

    int main(void) {
      std::cout << "Hello world!" << std::endl;
      return 0;
    }
    ```


Markdown syntax:

````markdown

=== "Unordered list"

    * Sed sagittis eleifend rutrum
    * Donec vitae suscipit est
    * Nulla tempor lobortis orci

=== "Ordered list"

    1. Sed sagittis eleifend rutrum
    2. Donec vitae suscipit est
    3. Nulla tempor lobortis orci


````

Which becomes:

=== "Unordered list"

    * Sed sagittis eleifend rutrum
    * Donec vitae suscipit est
    * Nulla tempor lobortis orci

=== "Ordered list"

    1. Sed sagittis eleifend rutrum
    2. Donec vitae suscipit est
    3. Nulla tempor lobortis orci



## Pre-formatted text with the `<pre>` HTML tag

To display pre-formatted text, use this syntax with the `<pre>` and `</pre>` HTML tags:
```markdown
<pre>

└── application
    └── templates
        ├── hzn-central
        └── hzn-edge
└── infrastructure
    └── templates
        ├── inf-devk8s-all
        ├── inf-gcp-central
        └── inf-k8s-edge
</pre>
```
Which becomes:

<pre>
└── application
    └── templates
        ├── hzn-central
        └── hzn-edge
└── infrastructure
    └── templates
        ├── inf-devk8s-all
        ├── inf-gcp-central
        └── inf-k8s-edge
</pre>

Here's another example:

```markdown
<pre>
Anthos location          | Pay-as-you-go (hourly) 
------------------------ | ---------------------- 
Google Cloud             | $0.01096 / vCPU        
On-Premises - bare metal | $0.03288 / vCPU 
</pre>
```

Which becomes:

<pre>

Anthos location          | Pay-as-you-go (hourly) 
------------------------ | ---------------------- 
Google Cloud             | $0.01096 / vCPU        
On-Premises - bare metal | $0.03288 / vCPU        

</pre>


## Highlighting text and changes, inline comments

You can also highlight suggested changes and add inline comments to a document. To see the syntax for each of the bullets below, use the code-copy button and paste the text into your Markdown document:

```markdown
* ==This was marked/highlighted==
* {++This was inserted++}
* {--This was deleted--}
* {>>This is an inline comment<<}

```

Which becomes:

* ==This was marked/highlighted==
* {++This was inserted++}
* {--This was deleted--}
* {>>This is an inline comment<<}

Here's another example. To see the syntax for the content below, use the code-copy button and paste the text into your Markdown document:

```markdown

Text can be {--deleted--} and replacement text {++added++}. ==Highlighting== is 
also possible {>>and comments can be added inline<<}.

{==

Formatting can also be applied to blocks by putting the opening and closing
tags on separate lines and adding new lines between the tags and the content.

==}

```

Which becomes:

Text can be {--deleted--} and replacement text {++added++}. ==Highlighting== is 
also possible {>>and comments can be added inline<<}.

{==

Formatting can also be applied to blocks by putting the opening and closing
tags on separate lines and adding new lines between the tags and the content.

==}

## Comments and TODO's

This topic outlines the syntax you can use for comments that won't appear in the generated output:

For example, you can use HTML syntax like this:
```markdown

<!---
your comment goes here
and here
-->

```

This type of comment will appear as a TODO item in your IDE (tested with IntelliJ and VS Code):

```markdown

<!-- TODO: Add more info here! -->

```


## Diagrams

This section outlines the various types of diagrams for your project documentation.  For more information on *mermaid.js* diagrams,
you can refer to this [documentation](https://mermaid.js.org/intro/){:target="_blank"}.


### PlantUML

PlantUML Markdown syntax example:

````markdown

```puml
@startuml
ORD -> G2P: CancelPickCommand
G2P -> WRK: RemoveWorkLoadUnitCommand
G2P -> WRK: RemoveWorkCommand
G2P-> ORD: ConfirmFulfillmentOrderLineCommand
G2P-> UI: PickCancelledEvent
UI -> G2P: ConfirmPickContainer
G2P -> ORD: ConfirmFulfillmentLoadUnitCommand
@enduml
```
````
Which becomes:

```puml
@startuml
ORD -> G2P: CancelPickCommand
G2P -> WRK: RemoveWorkLoadUnitCommand
G2P -> WRK: RemoveWorkCommand
G2P-> ORD: ConfirmFulfillmentOrderLineCommand
G2P-> UI: PickCancelledEvent
UI -> G2P: ConfirmPickContainer
G2P -> ORD: ConfirmFulfillmentLoadUnitCommand
@enduml
```

Another more elaborate example:
````markdown
```puml
@startuml
activate InventorySelector
InventorySelector -> InventorySelector : loadAllocationZones()
InventorySelector -> SelectorProvider : getSelectorInstance(RetrievalType.DEFAULT)
InventorySelector <- SelectorProvider : ShippingSelector
InventorySelector -> AbstractSelector : select()
activate AbstractSelector #DarkSalmon
AbstractSelector -> AbstractSelector : loadPreferenceConfigurations()
AbstractSelector -> PreferenceProvider : getPreferencesInstances(preferenceConfiguration)
AbstractSelector -> RestrictionProvider : getRestrictionInstances()
AbstractSelector -> PreferenceProcessor : process(restrictions, preferences, allocationZones, preferenceConfiguration)
activate PreferenceProcessor
loop for each Restriction
PreferenceProcessor -> Restriction : applyCriteria()
activate Restriction
PreferenceProcessor <- Restriction : CriteriaQuery
deactivate Restriction
end
loop for each Preference
PreferenceProcessor -> Preference : applyCriteria
activate Preference
PreferenceProcessor <- Preference : CriteriaQuery
deactivate Preference
end
PreferenceProcessor -> PreferenceProcessor : executeQuery
AbstractSelector <- PreferenceProcessor : return inventoryUnits
deactivate PreferenceProcessor
AbstractSelector -> ShippingSelector : buildResultSet(inventoryUnits)
AbstractSelector <- ShippingSelector : selectionResultDtos
InventorySelector <- AbstractSelector : selectionResultDtos
deactivate AbstractSelector
deactivate InventorySelector
@enduml
```
````
Which becomes:

```puml
@startuml
activate InventorySelector
InventorySelector -> InventorySelector : loadAllocationZones()
InventorySelector -> SelectorProvider : getSelectorInstance(RetrievalType.DEFAULT)
InventorySelector <- SelectorProvider : ShippingSelector
InventorySelector -> AbstractSelector : select()
activate AbstractSelector #DarkSalmon
AbstractSelector -> AbstractSelector : loadPreferenceConfigurations()
AbstractSelector -> PreferenceProvider : getPreferencesInstances(preferenceConfiguration)
AbstractSelector -> RestrictionProvider : getRestrictionInstances()
AbstractSelector -> PreferenceProcessor : process(restrictions, preferences, allocationZones, preferenceConfiguration)
activate PreferenceProcessor
loop for each Restriction
PreferenceProcessor -> Restriction : applyCriteria()
activate Restriction
PreferenceProcessor <- Restriction : CriteriaQuery
deactivate Restriction
end
loop for each Preference
PreferenceProcessor -> Preference : applyCriteria
activate Preference
PreferenceProcessor <- Preference : CriteriaQuery
deactivate Preference
end
PreferenceProcessor -> PreferenceProcessor : executeQuery
AbstractSelector <- PreferenceProcessor : return inventoryUnits
deactivate PreferenceProcessor
AbstractSelector -> ShippingSelector : buildResultSet(inventoryUnits)
AbstractSelector <- ShippingSelector : selectionResultDtos
InventorySelector <- AbstractSelector : selectionResultDtos
deactivate AbstractSelector
deactivate InventorySelector
@enduml
```


### Flowcharts

Use this Markdown syntax:

````markdown

``` mermaid
graph LR
  A[Start] --> B{Error?};
  B -->|Yes| C[Hmm...];
  C --> D[Debug];
  D --> B;
  B ---->|No| E[Yay!];
```

````
Which becomes:

``` mermaid
graph LR
  A[Start] --> B{Error?};
  B -->|Yes| C[Hmm...];
  C --> D[Debug];
  D --> B;
  B ---->|No| E[Yay!];
```

### Sequence diagrams

Markdown syntax:

````markdown

``` mermaid
sequenceDiagram
  autonumber
  Alice->>John: Hello John, how are you?
  loop Healthcheck
      John->>John: Fight against hypochondria
  end
  Note right of John: Rational thoughts!
  John-->>Alice: Great!
  John->>Bob: How about you?
  Bob-->>John: Jolly good!
```


````

Which becomes:

```mermaid
sequenceDiagram
  autonumber
  Alice->>John: Hello John, how are you?
  loop Healthcheck
      John->>John: Fight against hypochondria
  end
  Note right of John: Rational thoughts!
  John-->>Alice: Great!
  John->>Bob: How about you?
  Bob-->>John: Jolly good!
```


### State diagrams

Markdown syntax:

````markdown

```mermaid
stateDiagram-v2
  state fork_state <<fork>>
    [*] --> fork_state
    fork_state --> State2
    fork_state --> State3

    state join_state <<join>>
    State2 --> join_state
    State3 --> join_state
    join_state --> State4
    State4 --> [*]
```


````

Which becomes:

``` mermaid
stateDiagram-v2
  state fork_state <<fork>>
    [*] --> fork_state
    fork_state --> State2
    fork_state --> State3

    state join_state <<join>>
    State2 --> join_state
    State3 --> join_state
    join_state --> State4
    State4 --> [*]
```


### Class diagrams

Markdown syntax:

````markdown

``` mermaid
classDiagram
  Person <|-- Student
  Person <|-- Professor
  Person : +String name
  Person : +String phoneNumber
  Person : +String emailAddress
  Person: +purchaseParkingPass()
  Address "1" <-- "0..1" Person:lives at
  class Student{
    +int studentNumber
    +int averageMark
    +isEligibleToEnrol()
    +getSeminarsTaken()
  }
  class Professor{
    +int salary
  }
  class Address{
    +String street
    +String city
    +String state
    +int postalCode
    +String country
    -validate()
    +outputAsLabel()  
  }
```
````

Which becomes:

``` mermaid
classDiagram
  Person <|-- Student
  Person <|-- Professor
  Person : +String name
  Person : +String phoneNumber
  Person : +String emailAddress
  Person: +purchaseParkingPass()
  Address "1" <-- "0..1" Person:lives at
  class Student{
    +int studentNumber
    +int averageMark
    +isEligibleToEnrol()
    +getSeminarsTaken()
  }
  class Professor{
    +int salary
  }
  class Address{
    +String street
    +String city
    +String state
    +int postalCode
    +String country
    -validate()
    +outputAsLabel()  
  }
```

Another example:

```mermaid
classDiagram
Class01 <|-- AveryLongClass : Cool
Class03 *-- Class04
Class05 o-- Class06
Class07 .. Class08
Class09 --> C2 : Where am i?
Class09 --* C3
Class09 --|> Class07
Class07 : equals()
Class07 : Object[] elementData
Class01 : size()
Class01 : int chimp
Class01 : int gorilla
Class08 <--> C2: Cool label

```


### Entity-relationship diagrams

Markdown syntax:

````markdown

``` mermaid
erDiagram
  CUSTOMER ||--o{ ORDER : places
  ORDER ||--|{ LINE-ITEM : contains
  LINE-ITEM {
    string name
    int pricePerUnit
  }
  CUSTOMER }|..|{ DELIVERY-ADDRESS : uses
```


````

Which becomes:

``` mermaid
erDiagram
  CUSTOMER ||--o{ ORDER : places
  ORDER ||--|{ LINE-ITEM : contains
  LINE-ITEM {
    string name
    int pricePerUnit
  }
  CUSTOMER }|..|{ DELIVERY-ADDRESS : uses
```

### Git graphs

Markdown syntax:

````markdown

```mermaid

gitGraph
    commit
    commit
    branch develop
    checkout develop
    commit
    commit
    checkout main
    merge develop
    commit
    commit
```

````

Which becomes:

```mermaid

gitGraph
    commit
    commit
    branch develop
    checkout develop
    commit
    commit
    checkout main
    merge develop
    commit
    commit
```

Markdown syntax:

````markdown

```mermaid

gitGraph
    commit id: "Alpha"
    commit id: "Beta"
    commit id: "Gamma"
```


````

Which becomes:

```mermaid

gitGraph
    commit id: "Alpha"
    commit id: "Beta"
    commit id: "Gamma"
```


### Pie charts


Markdown syntax:

````markdown

```mermaid
pie title Pets adopted by volunteers
    "Dogs" : 386
    "Cats" : 85
    "Rats" : 15
```
````
Which becomes:

```mermaid

pie title Pets adopted by volunteers
    "Dogs" : 386
    "Cats" : 85
    "Rats" : 15
```

### Gantt charts

Markdown syntax:

````markdown


```mermaid
gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD
    section Section
    A task           :a1, 2014-01-01, 30d
    Another task     :after a1  , 20d
    section Another
    Task in sec      :2014-01-12  , 12d
    another task      : 24d
```

````
Which becomes:

```mermaid
gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD
    section Section
    A task           :a1, 2014-01-01, 30d
    Another task     :after a1  , 20d
    section Another
    Task in sec      :2014-01-12  , 12d
    another task      : 24d
```



### User journeys

Markdown syntax:

````markdown

```mermaid
journey
    title My work    
      Make tea: 5: Me
      Go upstairs: 3: Me
      Do work: 1: Me, Cat
    section Go home
      Go downstairs: 5: Me
      Sit down: 5: Me
```
````

```mermaid
journey
    title My work    
      Make tea: 5: Me
      Go upstairs: 3: Me
      Do work: 1: Me, Cat
    section Go home
      Go downstairs: 5: Me
      Sit down: 5: Me
```

### Gliffy

Gliffy diagrams aren't supported by the Mkdocs framework. You can export the *.png* file from Gliffy and display the diagram as an image in this documentation.

### Visio

Viso diagrams are not supported by the Mkdocs framework.  You can export the *.png* file from Visio and display
the diagram as an image in this documentation.

## Emojis

Here are some emoji examples.  You can find the full database of 10,000 available emojis 
and icons here: [https://squidfunk.github.io/mkdocs-material/reference/icons-emojis/#search](https://squidfunk.github.io/mkdocs-material/reference/icons-emojis/#search){:target="_blank"}

```markdown
- This content is way cool! :smile:
- Love this! :heart:
- The quick :brown_square: :fox: jumps over the lazy :dog:.
- My :brain: is exploding after reading this topic.
- Way to go! :thumbsup:
```

Which becomes:

- This content is way cool! :smile:
- Love this! :heart:
- The quick :brown_square: :fox: jumps over the lazy :dog:
- My :brain: is exploding after reading this topic.
- Way to go! :thumbsup:

## Footnotes

Text with footnote references:

```markdown

This is my text with footnotes[^1], some more text with another footnote[^2].

```

Which becomes:

This is my text with footnotes[^1], some more text with another footnote[^2].

### Adding the footnote content

Short footnotes are written on a single line:
```markdown

[^1]: This is the text for footnote #1. This is the text for footnote #1. This is the text for footnote #1.

```

[^1]: This is the text for footnote #1. This is the text for footnote #1. This is the text for footnote #1.

Paragraphs can be written on the next line and must be indented by four spaces:

```markdown

[^2]:
This is the text for footnote #2. This is the text for footnote #2. This is the text for footnote #2.
This is the text for footnote #2. This is the text for footnote #2.

```

[^2]:
This is the text for footnote #2. This is the text for footnote #2. This is the text for footnote #2.
This is the text for footnote #2. This is the text for footnote #2.

General block handling.

{--

* test remove
* test remove
* test remove
    * test remove
* test remove

--}

{++

* test add
* test add
* test add
    * test add
* test add

++}



## Markdown cheat sheets

There are plenty of good Markdown resources out there:

* [GitHub Markdown Guide](https://docs.github.com/en/github/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax){:target="_blank"}
* [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet){:target="_blank"}
* This one is mostly relevant as well: [Markdown Guide](https://www.markdownguide.org/){:target="_blank"}