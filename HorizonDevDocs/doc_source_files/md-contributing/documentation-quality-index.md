You can use linting tools in most modern IDEs to test the quality of the documentation and 
reduce typos, grammar issues, and other problems before you publish your documentation. This section outlines
a few tools you can use for this task.

## Vale as a linting tool for documentation quality

Vale is a popular open-source linting tool primarily used for checking and enforcing writing and style guidelines in technical documentation. Vale is a useful tool for proofreading and maintaining consistency in technical documentation. It can also be a useful tool for writers with English as a second language.

### Vale in IntelliJ

Vale is available in IntelliJ as a plugin.

**Steps**:

1. You can download the latest installation of Vale from here: [https://vale.sh/](https://vale.sh/){:target="_blank"}
2. In IntelliJ, install the **Grazie Pro** plugin:

    ![](../images/vale-6.png)

3. Configure the plugin with your installation of Vale:

    ![](../images/vale-1.png)
    ![](../images/vale-2.png)
    
4. Add the Vale installation to your `Path` variable. The Vale installation may do this automatically:

    ![](../images/vale-3.png)

5. Add the `vale.ini` file found in this repository to your repository. You must also update the `StylesPath` in the file.

    !!! note

        It's important that you place the styles outside your Git repository since these files shouldn't be committed to your repository.

    ![](../images/vale-4.png)

6. You may have to sync the `vale.ini` file in IntelliJ. After syncing the file, you can take full advantage of Vale language checking:

    ![](../images/vale-5.png)

### Vale in Visual Studio Code

Vale is available in Visual Studio Code as a plugin.

**Steps**:

1. Install the `Vale VSCode` plugin by Chris Chinchilla.
2. Configure all tabs (user, workspace, project folder) of the plugin like this to point to the `vale.exe` binary and the `vale.ini` on your local machine.

    ![Alt text](../images/vale-configuration-1.png)

2. Results of the linting are shown on the **Problems** tab of Visual Studio Code.

## LTeX in Visual Studio Code

The LTeX plugin will also lint or test your documentation for grammar and spelling errors, as well as stylistic errors such as passive voice and other issues. It's also available as a plugin in Visual Studio Code.

**Steps**:

1. Install the LTeX Language Tool plugin in Visual Studio Code:
   
    ![](../images/LTeX-plugin-vscode.png)

2. Make this configuration change in the plugin settings:

    ![](../images/LTeX-plugin-vscode-settings.png)

3. You will see the results of linting (grammar, typos, etc.) on the **Problems** tab of Visual Studio Code:

    ![](../images/problems-tab.png)

    Another example:

    ![](../images/latext.png)
