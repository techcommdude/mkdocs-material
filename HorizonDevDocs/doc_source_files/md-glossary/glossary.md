This section has information and links to the terminology and technologies used to deploy Project Horizon.


## Application

<!-- TODO: Still need to do.) -->

:   In the context of Project Horizon, **Application** is....

## Central

:   In the context of Project Horizon, Central is the part of the solution that's primarily intended to be installed in the cloud. 
This isn't a hard rule but a general description. This includes things like a database (Postgres), Kubernetes cluster, 
cache (Redis), messaging (Nats), as well as Horizon SCMs.


## Edge

:   In the context of Project Horizon, **Edge** is the portion of the solution that needs to be close to the warehouse hardware. This is useful for low latency
responses without necessarily needing to go all the way to `Central` to get an answer. This part of the deployment has
things like messaging (Nats), a cache (Redis), and edge Horizon SCMs.

## GKE - Google Kubernetes Engine

:   Google Kubernetes Engine (GKE) is a managed Kubernetes service that you can use to deploy and operate containerized applications 
at scale using Google's infrastructure. GKE is a scalable, automated, 
managed Kubernetes solution. You should already be familiar 
with [Kubernetes concepts](https://kubernetes.io/docs/concepts/overview/components/){:target="_blank"}.

## Google Cloud Platform (GCP)

:   Google Cloud Platform (GCP) is a **public cloud computing platform** that offers a wide range of services such as compute, storage, networking, application development, Big Data, and more. It is built on the same cloud infrastructure that Google uses internally for its end-user products like Google Search, Photos, Gmail, and YouTube. GCP provides a variety of services that can be mixed and matched to create the infrastructure you need for your website or application. 

:   Google Cloud consists of physical assets like computers and hard disk drives, as well as virtual resources like virtual machines (VMs). These resources are located in Google's data centers around the world. The data centers are organized into regions, which are available in Asia, Australia, Europe, North America, and South America. Each region contains zones that are isolated from each other within the region. This distribution of resources provides benefits such as redundancy in case of failure and reduced latency by locating resources closer to clients.

:   You can access these resources through various services offered by Google Cloud. These services allow you to mix and match different components to create the infrastructure you need for your specific use case. Some resources can be accessed globally across regions and zones, while others can only be accessed within the same region or zone.

:   To interact with Google Cloud services, you can use the Google Cloud console, command-line interface (CLI), or client libraries. The list of available services is extensive and continues to grow as Google Cloud evolves.

:   For more detailed information about Google Cloud Platform and its features, you can refer to the official documentation.

:   For more information:

:   * [Google Cloud Platform Tutorial - Javatpoint](https://www.javatpoint.com/google-cloud-platform){:target="_blank"}
:   * [Google Cloud overview | Overview](https://cloud.google.com/docs/overview/){:target="_blank"}
:   * [Google Cloud Platform: A cheat sheet | TechRepublic](https://www.techrepublic.com/article/){:target="_blank"}

## Grafana

:  Grafana is an open-source, web-based platform for monitoring and observability. It's commonly used for visualizing and analyzing metrics and logs from various data sources. Grafana provides a user-friendly interface for creating interactive, real-time dashboards and charts, which are particularly useful for monitoring the performance of systems, applications, and infrastructure.

:   Grafana is commonly used in DevOps, IT operations, and other fields where monitoring and observability of systems and applications are essential. It's a versatile tool that can be integrated into various monitoring and alerting workflows, making it easier to maintain the health and performance of complex systems.

## Helm

:   Helm is a package manager for Kubernetes applications. It uses a packaging format called charts, which are collections of files that describe a related set of Kubernetes resources. A chart can be used to deploy anything from simple pods to complex web app stacks with HTTP servers, databases, caches, and more. Helm automates the creation, packaging, configuration, and deployment of Kubernetes applications by combining your configuration files into a single reusable package.

:   A chart is organized as a collection of files inside a directory. The directory name is the name of the chart, and it contains files such as `Chart.yaml`, `LICENSE`, `README.md`, `values.yaml`, `values.schema.json`, `charts/`, `crds/`, and `templates/`. The `Chart.yaml` file is required and contains information about the chart, such as its API version, name, version, description, and more.

:   Helm charts let you package collections of Kubernetes manifests as complete applications ready for deployment. You can create templated configurations that end users can easily modify before installing a release into a cluster.

:   For more detailed information about Helm and how to get started writing Helm charts for your Kubernetes applications, you can refer to the following resources:

:   * [Helm | Charts](https://helm.sh/docs/topics/charts/){:target="_blank"}
:   * [How to Get Started Writing Helm Charts for Your Kubernetes Applications](https://www.howtogeek.com/devops/how-to-get-started-writing-helm-charts-for-your-kubernetes-applications/){:target="_blank"}
:   * [Using Helm and Kubernetes | Baeldung](https://www.baeldung.com/ops/kubernetes-helm){:target="_blank"}
:   * [https://circleci.com/blog/what-is-helm/](https://circleci.com/blog/what-is-helm/){:target="_blank"}.
:   * [https://helm.sh/](https://helm.sh/){:target="_blank"}


## Helm chart

:   A Helm chart is a package that contains all the necessary resources to deploy an application to a Kubernetes cluster. This includes 
YAML configuration files for deployments, services, secrets, and config maps that define the desired state of your application.

:   A Helm chart packages together YAML files and templates that generate more configuration files based on 
parametrized values. You can then customize configuration files to suit different environments and to create reusable 
configurations for use across many deployments. Additionally, you can version and independently manage each Helm chart, making it 
possible to keep many versions of an application with different configurations.

:   For more information, see [https://circleci.com/blog/what-is-helm/](https://circleci.com/blog/what-is-helm/){:target="_blank"}.

## Infrastructure

:   In the context of Project Horizon, **Infrastructure** is ...

## K8s - Kubernetes

:   [Kubernetes](https://kubernetes.io/){:target="_blank"}, also known as *K8s*, is an open-source system for automating deployment, scaling, and management of containerized applications. It groups containers that make up an application into logical units for ease of management and discovery.

## kubectl

:   Kubectl is a command line tool for communicating with a Kubernetes cluster's control plane, using the Kubernetes API.
It allows you to perform various operations on the cluster resources and applications,
such as creating, deleting, scaling, inspecting, and troubleshooting.
Kubectl also supports configuration files in YAML or JSON formats,
which can be used to specify the desired state of the cluster.

## Nats messaging

<!--  TODO: Still need to do this.) -->

:   TODO


## NGINX - Ingress controllers

:   An [Ingress controller](https://www.nginx.com/resources/glossary/kubernetes-ingress-controller/){:target="_blank"} is a specialized load balancer for Kubernetes (and other containerized) environments. Kubernetes is the de facto standard 
for managing containerized applications. An Ingress controller abstracts away the complexity of Kubernetes application traffic 
routing and provides a bridge between Kubernetes services and external ones.

## Postgres/PostgreSQL database

:   PostgreSQL, often referred to as Postgres, is an open-source relational database management system (RDBMS). It is known for its robustness, extensibility, and SQL compliance. PostgreSQL is widely used for data storage, retrieval, and management in various applications and is particularly popular among developers and organizations for its advanced features and reliability.

:   PostgreSQL is commonly used for a wide range of applications, from small projects to large enterprise systems, and it has earned a reputation for its stability and reliability. It is often the RDBMS of choice for those who value open-source solutions and advanced database capabilities.

## Redis
:  Redis, short for "REmote DIctionary Server," is an open-source, in-memory data store that is often used as a cache or message broker in various software applications. It is a key-value database that stores data in memory, making it extremely fast for read-heavy operations. Redis is commonly used for caching because of its high-performance characteristics.

:   Redis is widely used in web applications to speed up data access and reduce the load on backend databases. It is also utilized in real-time analytics, session management, and more. It has gained popularity for its simplicity, speed, and versatility in various use cases, particularly as a caching solution.

## SCM — Self Contained Module


:   In Project Horizon, the Self Contained Module (SCM) approach is an architecture that focuses on a separation of the functionality into many independent systems, making the complete logical system a collaboration of many smaller software systems.

:   The characteristics of an SCM are:

  * Each SCM provides a cohesive business functionality (including logic and data) which is offered by well-defined interfaces which hide all internal implementation details.
  * Each SCM is owned by one team at Dematic. This does not necessarily mean that only one team can change the code, but the owning team has the final say on what goes into the code base, for example by merging pull-requests.
  * Communication with other SCMs is asynchronous whenever possible. This decouples the systems, reduces the effects of failure, and thus supports autonomy.
  * An SCM might have its own UI to visualize business processes provided by the SCM.
  * SCM do not share business code with other SCMs. It is fine to common (technical) libraries and client libraries form other SCMs.
  * SCM can be deployed independently of each other which implies that an SCM must be able to cope with the situation that its partner SCMs are not available.

  * In the literature often the notion "microservice" is used. Since this notion is somehow fuzzy the notion SCM is used as defined above.

## Terraform

:   Terraform is an open-source “Infrastructure as Code” tool developed by HashiCorp. It allows developers to use a high-level configuration language called HCL (HashiCorp Configuration Language) to describe the desired “end-state” cloud or on-premises infrastructure for running an application. Terraform simplifies the user experience by enabling users to specify the expected state of resources without the need to specify the exact steps to achieve the desired state of resources. It manages how the infrastructure needs to be modified to achieve the desired result.

:   Terraform is capable of defining both cloud and on-premises resources in human-readable configuration files that can be versioned, reused, and shared. It provides a consistent workflow for provisioning and managing infrastructure throughout its lifecycle. Terraform can manage low-level components such as compute, storage, and networking resources, as well as high-level components like DNS entries and SaaS features.

:   Terraform is a powerful IaC tool that simplifies the management of infrastructure by allowing you to define, provision, and maintain resources across various cloud and on-premises environments in a consistent and automated manner. It's widely used in the DevOps and cloud computing space to streamline infrastructure operations.

:   For more information:

:   * [https://developer.hashicorp.com/terraform/intro](https://developer.hashicorp.com/terraform/intro){:target="_blank"}
:   * [https://www.ibm.com/topics/terraform](https://www.ibm.com/topics/terraform){:target="_blank"}

## Templates in Terraform

:   In Terraform, templates typically refer to the concept of using variables and interpolation to generate dynamic configuration values. This allows you to create reusable and parameterized configurations by using placeholders that are filled in with actual values during the execution of your Terraform code.

:   Terraform uses a language called HashiCorp Configuration Language (HCL) to define infrastructure as code. In HCL, you can define variables, and then you can use these variables to create dynamic configurations. Templates in Terraform typically involve using variables and interpolation to construct configuration values or to make your configuration more flexible.

:   Here are some common use cases for templates in Terraform:

:   * **Variable Interpolation**: You can use variables and interpolate their values within your configuration to make it more dynamic. For example, you might define a variable for a Google Cloud Platform (GCP) region and then use that variable to specify the region for various resources.

```hcl
variable "google_cloud_region" {
  description = "The GCP region to use."
}

provider "gcp" {
  region = var.gcp_region
}
```

:   * **Resource Configuration**: You can use variables and interpolation to generate resource configurations based on user input or other dynamic factors. For example, you might use a variable to define the number of EC2 instances to launch, and then use a count argument to create multiple instances.

```hcl
variable "instance_count" {
  description = "The number of EC2 instances to launch."
  type        = number
  default     = 2
}

resource "gcp_instance" "example" {
  count = var.instance_count
  ami   = "ami-12345678"
  instance_type = "t2.micro"
  // ...
}
```

:   * **Template Files**: Terraform can also use external template files to generate configuration content. You can use the templatefile function to read and interpolate values from an external template file. This is particularly useful for generating files like cloud-init user data scripts.

```hcl
resource "aws_instance" "example" {
  ami           = "ami-12345678"
  instance_type = "t2.micro"
  user_data = templatefile("userdata.tpl", { key1 = "value1", key2 = "value2" })
}

```

:   Terraform's ability to use variables and interpolation makes it a powerful tool for managing infrastructure as code. It allows you to create flexible and reusable configurations that can adapt to different environments and requirements.

## Vault by Hashicorp

:   HashiCorp Vault is an identity-based secrets and encryption management system. It provides encryption services that are gated by authentication and authorization methods to ensure secure, auditable, and restricted access to secrets1. Secrets can include API encryption keys, passwords, and certificates. You can use Vault to secure, store, and protect secrets and other sensitive data using a UI, CLI, or HTTP API.

:   Vault offers a unified interface to any secret while providing tight access control and recording a detailed audit log. It can be used to manage secrets such as tokens, API keys, passwords, encryption keys, or certificates. Vault validates and authorizes clients (users, machines, apps) before providing them access to secrets or stored sensitive data.

:   Vault works primarily with tokens. A token is associated with the client’s policy. Each policy is path-based and policy rules constrain the actions and accessibility to the paths for each client. The core Vault workflow consists of four stages: Authenticate, Validation, Authorize, and Access.

:   For more information about HashiCorp Vault, you can refer to the official [documentation](https://developer.hashicorp.com/vault/docs/what-is-vault){:target="_blank"}.