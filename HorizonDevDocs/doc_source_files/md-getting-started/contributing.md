Anyone with a GitLab account can contribute to the documentation. To contribute, you only need to request access to [this project](https://gitlab.dematic.com/gst/docusaurus-static-site){:target="_blank"} in GitLab.

### Editing documentation directly with the GitLab Web IDE

Using this method you don't have to clone or fork the project to contribute changes, you only have to create a temporary branch with your changes. 

The GitLab Web IDE (cloud version of VS Code) allows you to directly edit (and preview) the source files in Markdown format.

!!! note

    This site always contains the latest version of the documentation that has been committed to the `main` branch of the project.

To suggest changes by directly editing a documentation source file for this project:

1. Click the **Edit this page** icon ![Edit this page icon](../images/Edit_this_page_icon.png)
   located in the upper-right corner of each topic.
2. Click **Edit** and then select **Open in Web IDE**:

    ![Open Web IDE](../images/Web_IDE_GitLab1.png)

3. Now you can edit the content and preview the output:

    ![Web IDE](../images/Web_IDE_GitLab.png)

    !!! tip
   
        For examples of appropriate Markdown syntax, see [Markdown syntax examples](../md-contributing/markdown-sample.md).

4. Save your changes and select the source control icon ![Source control icon](../images/SourceControlIcon.png).
5. Enter a commit message by clicking the dropdown arrow from the **Commit to main** button and selecting **Commit to new branch**. You can use the default branch name and press **Enter** on your keyboard to confirm:

    ![Commit to a new branch](../images/CommitToNewBranch.png)

6. When prompted, create the Merge Request (MR) in GitLab:

    ![](../images/MergeRequestGitlab.png)

7. Complete these fields in GitLab and click **Create Merge Request**:

    ![](../images/MRGitlab1.png)
    ![](../images/MRGitlab3.png)

    !!! note

           Add every member of the **Doc Team** group as a reviewer for fastest turnaround on your MR. To view members of the Doc Team select the **aproval rules** dropdown.

8. The [documentation team](mailto:geoffrey.farnell@kiongroup.com; kyle.gunderson@kiongroup.com; katherine.baeckeroot@kiongroup.com) will review your request and merge the changes into the `main` branch after review.

!!! tip
   
      You can also add content (preferably in Markdown format) directly to the repository. As part of your MR (merge request), please let us know how to incorporate your suggestions in the overall documentation.

### Contact the documentation team

Click the button below to contact the documentation team directly by email. Our team is also available on Microsoft Teams if you have any suggestions or questions. 

Feel free to send us content and suggestions in any format. If you have a suggestion for one or more topics, please provide links to the help topics and outline your proposed changes in as much detail as possible.

The Technical Writing team is happy to review any suggestions you may have.

[:material-email: Email the documentation team :material-email:](mailto:geoffrey.farnell@kiongroup.com; kyle.gunderson@kiongroup.com; katherine.baeckeroot@kiongroup.com){ .md-button .md-button--primary }

### Create a GitLab documentation issue

Click the button below to create a GitLab documentation issue for the project.

The documentation team will review your request and make the appropriate changes.

[:simple-gitlab: Create a GitLab Issue for documentation :simple-gitlab:](https://gitlab.dematic.com/gst/docusaurus-static-site/-/issues/new?issue[description]=Provide%20a%20link%20to%20the%20documentation%20and%20describe%20what%20is%20wrong%20with%20it.%0A%0A%3C!--%20Do%20not%20alter%20the%20line%20below%20as%20it%20will%20assign%20the%20issue%20to%20reviewers%20on%20the%20Doc%20team:%20%20--%3E%0A%0A/assign%20@A0088315%20@A0083280%0A%0A%3C!--%20Paste%20URL%20or%20link%20to%20docs%20below%20this%20line%20%20--%3E%0A%0A&issue[title]=Docs%20feedback:%20Write%20your%20title%20here){:target="_blank" .md-button .md-button--primary}