The baseline architecture and overall system structure is currently documented on the Project Horizon wiki. For more information, see these links:

* [https://wiki.dematic.com/display/IQH/Baseline+architecture](https://wiki.dematic.com/x/nJceHQ){:target="_blank"}
* [https://wiki.dematic.com/display/IQH/System+structure](https://wiki.dematic.com/x/npceHQ){:target="_blank"}
* [https://wiki.dematic.com/display/IQH/System+layers+and+SCMs](https://wiki.dematic.com/x/HgiFJ){:target="_blank"}