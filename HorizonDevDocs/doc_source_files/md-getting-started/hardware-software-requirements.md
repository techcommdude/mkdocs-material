!!! warning

    This section is currently under construction.

This section outlines hardware and software requirements for both local and cloud-based deployments. Local deployments are often utilized for testing and development purposes. You may want to test a feature or test the deployment process locally before deploying to cloud.

A cloud deployment is often a customer project that's in development, testing, or production phase.

## Hardware requirements (local deployments)

<!--  TODO: -->

### RAM

<!--  TODO: Docker can be very memory intensive.) -->

* 64 GB

### CPU

## Software requirements (local deployments)

<!--  TODO: -->

### Operating system (OS)

#### Windows

* Windows 10 or higher.
#### Windows Subsystem for Linux (Ubuntu)

<!--  TODO: Need this for cloud as well?) -->

* You will require a “Version 2” of WSL as outlined in [Installing WSL on Windows](../md-deployment/wsl.md#wsl-windows-subsystem-for-linuxubuntu-installation-and-configuration).

### Docker Desktop

* Version 24.06 or higher.

### PowerShell

* Version 7.3 or higher.

### Python

Primarily used for testing purposes.

<!--  TODO: -->


### Helm

* Version 3.13.0 or higher.

### Vault

### Terraform

* Version 1.4.4 or higher.


## Hardware requirements (cloud deployment)

<!--  TODO: -->

### RAM

<!--  TODO: If we are not using Docker here, do we need as much memory?) -->

* 64 GB

### CPU

## Software requirements (cloud deployment)

<!--  TODO: -->

### Operating System (OS)
#### Windows

* Windows 10 or higher.

#### Windows Subsystem for Linux (Ubuntu)

<!--  TODO: Need this for cloud as well?) -->

* You will require a "Version 2" of WSL as outlined in [Installing WSL on Windows](../md-deployment/wsl.md#wsl-windows-subsystem-for-linuxubuntu-installation-and-configuration).

### Helm

* Version 3.13.0 or higher.

### Vault

### Terraform

* Version 1.4.4 or higher.

### kubectl CLI for Kubernetes

* Version 1.2.82 or higher.

### Google Cloud (GCP)

<!--  TODO: Do we need a version here?) -->