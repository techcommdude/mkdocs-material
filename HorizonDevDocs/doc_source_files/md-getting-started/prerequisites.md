This section gives a brief overview of the key technologies in Project Horizon. You will need to learn more about
these tools as you work more with Horizon, but for now you can find a brief overview below.

## Kubernetes, kubectl, and GCP (Google Cloud Platform)

Basic knowledge of Kubernetes (k8s) encompasses various essential concepts such as containerization, pods, deployments, services, replica sets, labels and selectors, namespaces, scaling, config maps and secrets, persistent volumes and persistent volume claims, ingress, and the Kubernetes API.

kubectl is commonly used in Google Cloud Platform (GCP) for managing Kubernetes clusters. Kubernetes is an open-source container orchestration platform, and Google Kubernetes Engine (GKE) is a managed Kubernetes service provided by Google Cloud.

kubectl is the command-line tool used to interact with Kubernetes clusters, including those deployed on GCP using GKE. You can use kubectl to perform various tasks such as deploying and managing containerized applications, scaling workloads, inspecting cluster resources, and more on GKE clusters.

Some common kubectl commands and flags are:

* `kubectl get [TYPE] [NAME]` : Displays one or more resources of a specified type or name. For example, `kubectl get pods` shows all the pods in the current namespace, and `kubectl get pod my-pod` shows the details of a specific pod.
* `kubectl create -f [FILE]` : Creates a resource from a file or stdin. For example, `kubectl create -f pod.yaml` creates a pod from the pod.yaml file.
* `kubectl delete [TYPE] [NAME]` : Deletes a resource of a specified type or name. For example, `kubectl delete pod my-pod` deletes the my-pod pod.
* `kubectl describe [TYPE] [NAME]` : Shows detailed information about a resource of a specified type or name. For example, `kubectl describe pod my-pod` shows the status, events, and other information about the my-pod pod.
* `kubectl logs [POD] [CONTAINER]` : Prints the logs for a container in a pod. For example, `kubectl logs my-pod my-container` shows the logs for the my-container container in the my-pod pod.
* `kubectl exec [POD] [COMMAND]` : Executes a command in a container in a pod. For example, `kubectl exec my-pod -- ls /` runs the ls / command in the default container of the my-pod pod.

If you are working with Kubernetes clusters in GCP, you will likely use kubectl as a primary tool for managing and interacting with your clusters and applications.

For more information about kubectl syntax, operations, and examples:

* [kubectl cheat sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/){:target="_blank"}
* [kubectl reference documentation](https://kubernetes.io/docs/reference/){:target="_blank"}
* [https://kubernetes.io/docs/home/](https://kubernetes.io/docs/home/){:target="_blank"}

Other references for kubectl:

* [https://kubernetes.io/docs/tasks/tools/](https://kubernetes.io/docs/tasks/tools/){:target="_blank"}
* [https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands){:target="_blank"}

## Google Cloud Platform (GCP)

Google Cloud Platform (GCP) is a **public cloud computing platform** that offers a wide range of services such as compute, storage, networking, application development, Big Data, and more. GCP uses the same cloud infrastructure that Google uses internally for its end-user products like Google Search, Photos, Gmail, and YouTube. GCP provides a variety of services that can be mixed and matched to create the infrastructure you need for your website or application. 

Google Cloud consists of physical assets like computers and hard disk drives, as well as virtual resources like virtual machines (VMs). You can find these resources in Google's data centers around the world. The data centers are organized into regions, which are available in Asia, Australia, Europe, North America, and South America. Each region contains zones that are isolated from each other within the region. This distribution of resources provides benefits such as redundancy in case of failure and reduced latency by locating resources closer to clients.

You can access these resources through various services offered by Google Cloud. These services allow you to mix and match different components to create the infrastructure you need for your specific use case. You can access some resources globally across regions and zones, while access to others is within the same region or zone.

To interact with Google Cloud services, you can use the Google Cloud console, command-line interface (CLI), or client libraries. The list of available services is extensive and continues to grow as Google Cloud evolves.

For more detailed information about Google Cloud Platform and its features, you can refer to the official documentation.

For more information:

* [Google Cloud Platform Tutorial - Javatpoint](https://www.javatpoint.com/google-cloud-platform){:target="_blank"}
* [Google Cloud overview | Overview](https://cloud.google.com/docs/overview/){:target="_blank"}
* [Google Cloud Platform: A cheat sheet | TechRepublic](https://www.techrepublic.com/article/){:target="_blank"}


## Terraform

You will need a foundational understanding of Terraform includes, but is not restricted to, the following key aspects: Infrastructure as Code (IaC) principles, Terraform configuration files (HCL), providers, resources, modules, variables, outputs, state management, and Terraform CLI commands.
.
Terraform is an open-source “Infrastructure as Code” (IaC) tool developed by HashiCorp. Developers use a high-level configuration language called HCL (HashiCorp Configuration Language) to describe the desired “end-state” cloud or on-premises infrastructure for running an application. Terraform simplifies the user experience by enabling users to specify the expected state of resources without the need to specify the exact steps to achieve the desired state of resources. It manages how the modification of the infrastructure to achieve the desired result.

Terraform is capable of defining both cloud and on-premises resources in human-readable configuration files that can be versioned, reused, and shared. It provides a consistent workflow for provisioning and managing infrastructure throughout its lifecycle. Terraform can manage low-level components such as compute, storage, and networking resources, and high-level components like DNS entries and SaaS features.

Terraform is a powerful IaC tool that simplifies the management of infrastructure by allowing you to define, provision, and maintain resources across various cloud and on-premises environments in a consistent and automated manner. It's widely used in the DevOps and cloud computing space to streamline infrastructure operations.

For more information:

* [https://developer.hashicorp.com/terraform/intro](https://developer.hashicorp.com/terraform/intro){:target="_blank"}  
* [https://www.ibm.com/topics/terraform](https://www.ibm.com/topics/terraform){:target="_blank"}  
* [https://developer.hashicorp.com/terraform/downloads](https://developer.hashicorp.com/terraform/downloads){:target="_blank"} 
* [https://developer.hashicorp.com/terraform/docs](https://developer.hashicorp.com/terraform/docs){:target="_blank"} 

Some excellent tutorials from Terraform are available here:

* [Google Cloud (GCP) ](https://developer.hashicorp.com/terraform/tutorials/gcp-get-started){:target="_blank"} 
* [Manage Kubernetes with Terraform](https://developer.hashicorp.com/terraform/tutorials/kubernetes){:target="_blank"} 
* [Terraform and Helm](https://developer.hashicorp.com/terraform/tutorials/kubernetes/helm-provider){:target="_blank"} 

## Helm

Helm is a package manager for Kubernetes applications. It uses a packaging format called charts, which are collections of files that describe a related set of Kubernetes resources. A chart can be used to deploy anything from simple pods to complex web app stacks with HTTP servers, databases, caches, and more. Helm automates the creation, packaging, configuration, and deployment of Kubernetes applications by combining your configuration files into a single reusable package.

A chart is organized as a collection of files inside a directory. The directory name is the name of the chart, and it has files such as `Chart.yaml`, `LICENSE`, `README.md`, `values.yaml`, `values.schema.json`, `charts/`, `crds/`, and `templates/`. The required file of `Chart.yaml` has information about the chart, such as its API version, name, version, description, and more.

Helm charts let you package collections of Kubernetes manifests as complete applications ready for deployment. You can create templated configurations that end-users can change before installing a release into a cluster.

For more detailed information about Helm and how to get started writing Helm charts for your Kubernetes applications, you can refer to the following resources:

* [Helm | Charts](https://helm.sh/docs/topics/charts/){:target="_blank"}
* [How to Get Started Writing Helm Charts for Your Kubernetes Applications](https://www.howtogeek.com/devops/how-to-get-started-writing-helm-charts-for-your-kubernetes-applications/){:target="_blank"}
* [Using Helm and Kubernetes | Baeldung](https://www.baeldung.com/ops/kubernetes-helm){:target="_blank"}
* [https://circleci.com/blog/what-is-helm/](https://circleci.com/blog/what-is-helm/){:target="_blank"}.
* [https://helm.sh/](https://helm.sh/){:target="_blank"}


## Helm chart

A Helm chart is a package that contains all the necessary resources to deploy an application to a Kubernetes cluster. This includes YAML configuration files for deployments, services, secrets, and config maps that define the desired state of your application.

A Helm chart packages together YAML files and templates that generate more configuration files based on 
parametrized values. You can then customize configuration files to suit different environments and to create reusable 
configurations for use across many deployments. Additionally, you can version and independently manage each Helm chart, making it 
possible to keep many versions of an application with different configurations.

For more information, see [https://circleci.com/blog/what-is-helm/](https://circleci.com/blog/what-is-helm/){:target="_blank"}.

## Vault by HashiCorp

HashiCorp Vault is an identity-based secrets and encryption management system. A secret is anything that you want to control access to, such as API encryption keys, passwords, and certificates. Vault provides encryption services that are gated by authentication and authorization methods. Using Vault’s UI, CLI, or HTTP API, access to secrets and other sensitive data can be securely stored and managed, controlled (restricted), and auditable.

For more information, see [https://developer.hashicorp.com/vault/docs/what-is-vault](https://developer.hashicorp.com/vault/docs/what-is-vault){:target="_blank"}.

## Dematic certificate

If you are working with any of the internal Dematic URLs, you will need to install a Root Certificate Authority on your computer.

You can obtain the certificate from here:

* [https://gitlab.dematic.com/horizon/playground/nginx/-/blob/master/createcerts/DematicSPE_CA.pem](https://gitlab.dematic.com/horizon/playground/nginx/-/blob/master/createcerts/DematicSPE_CA.pem){:target="_blank"}

After downloading the file, you can install it using the Certificate Manager in Windows as a Trusted Root CA.