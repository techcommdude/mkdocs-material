This section has general information on roles that are typically involved with a project such as Horizon. This information originated on the Dematic wiki found [here](https://wiki.dematic.com/display/IQH/Roles+for+engineering+content+-+Project+Horizon#RolesforengineeringcontentProjectHorizon-Roles){:target="_blank"}.

Each role outlines some tasks that are typically part of the role.

## SAE project developer

<!--  TODO: -->

* Manual and automated testing.
* Customizing the system to meet customer's requirements/needs.
* Initial deployment and update of the deployment.
* Understanding of actual customer operational tasks (dropping orders, order picking, inventory mgmt, etc.)
    * Customer tasks that are covered in the product online documentation.
* Maintenance of Horizon admin tasks within Horizon
    * Purging the system.
    * System configuration.
    * User and role management/permissions.
    * Message maintenance.
* Maintains a debugging cookbook.
    * Extensive troubleshooting experience.
    * Troubleshooting/Debugging experience when things go wrong.
    * Understands what things look like when they're going right.
    * Understanding of tools (when available) used to troubleshoot (ex. parsing logs, digesting raw data for troubleshooting).
* Configuration experience (warehouse generation).

## QC tester (R&D / SAE)

<!--  TODO: -->

* Understanding of actual customer operational tasks (dropping orders, order picking, inventory mgmt, etc.)
    * Customer tasks that are covered in the product online documentation.
* Maintenance of Horizon admin tasks within Horizon
    * User and role management/permissions.
* Maintains a debugging cookbook.
    * Troubleshooting/Debugging experience when things go wrong.
    * Understands what things look like when they're going right.
    * Understanding of tools (when available) used to troubleshoot (ex. parsing logs, digesting raw data for troubleshooting).
* Configuration experience (warehouse generation).

## CS software support engineer

<!--  TODO: -->

* Understanding of actual customer operational tasks (dropping orders, order picking, inventory mgmt, etc.)
    * Customer tasks that are covered in the product online documentation.
* Maintenance of Horizon admin tasks within Horizon
    * Purging the system.
    * System configuration.
    * User and role management/permissions.
* Maintains a debugging cookbook.
    * Troubleshooting/Debugging experience when things go wrong.
    * Understands what things look like when they're going right.
    * Understanding of tools (when available) used to troubleshoot (ex. parsing logs, digesting raw data for troubleshooting).
* Understanding of the escalation process and when to escalate to GSO or R&D.

## SPE/R&D developer

<!--  TODO: -->

* Development based on product while trying to be solution agnostic.
* Unit & automated testing. 
* Capturing documentation for development coding style guides.
* Develop and deploy local sessions of different parts of systems.
    * Build their own solutions and/or deployments from the ground up.
* Deeper level of granularity; spend more time building new solutions.
    * GSO interfaces between R&D and a specific customer solution.
* Initial deployment and update of the deployment.
* Understanding of actual customer operational tasks (dropping orders, order picking, inventory mgmt, etc.)
    * Customer tasks that are covered in the product online documentation.
* Maintenance of Horizon admin tasks within Horizon
    * Purging the system.
    * System configuration.
    * User and role management/permissions.
* Maintains a debugging cookbook.
    * Extensive troubleshooting experience.
    * Troubleshooting/Debugging experience when things go wrong.
    * Understands what things look like when they're going right.
    * Understanding of tools (when available) used to troubleshoot (ex. parsing logs, digesting raw data for troubleshooting).
* Configuration experience (warehouse generation).
* Functional Documentation for:
    * SCMs.
    * Subsystems.