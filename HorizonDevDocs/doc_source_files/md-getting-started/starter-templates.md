
Terraform templates are used to deploy and manage your project. You can download the standard Terraform templates (maintained by Dematic Development) for your solution or customer project to your local system by using the `update-templates.ps1` PowerShell script found in this repository:

* [https://gitlab.dematic.com/horizon/solution/templates/solution](https://gitlab.dematic.com/horizon/solution/templates/solution)

As a first step, you can fork the above repository in GitLab. At that point you can run the script and download the Terraform templates that are used to deploy and manage your customer project. The comments in the solution templates will guide you through the basics of creating your own solution or customer project. This documentation will also help you to create your customer project.

<!--  TODO: There are two paths here:  Just download the templates into a new project, or go through the mcf-dev-local deployment which is a sample.) -->

For more information on how to download the Terraform templates and start building your own customer project, see [Creating your own customer project](../md-deployment/customer-project.md).

As a useful starting point, you can also go through the process of deploying a demo project locally using Docker Desktop and a local Kubernetes cluster or in the cloud with GCP:

* [Local Kubernetes cluster deployment demo project](../md-deployment/local-setup.md)
* [Google Cloud deployment demo project](../md-deployment/google-cloud-deployment.md)

## What does a deployment look like

A full deployment consists of four separate but interconnected components. All these components are deployed and managed
via Terraform configurations.

1. **Infrastructure Central**—Infrastructure typically (but not always) deployed to the cloud.
   
2. **Infrastructure Edge**—Infrastructure typically deployed to an on-premises Kubernetes (K8s) cluster.
   
3. **Horizon Central**—Horizon SCMs deployed to the `Central` infrastructure.
   
4. **Horizon Edge**—Horizon SCMs deployed to the `Edge` infrastructure.

## What is `Central`

`Central` is the portion of a solution that's primarily intended to be installed in the cloud. This isn't a hard rule, but a general practice. This usually includes a database, Kubernetes cluster, cache, messaging, and Horizon SCMs.

`Central` is where most of the business processing occurs. It's the central component of the whole deployment.

<!--  TODO: Should Infrastructure and Application be capitalized?) -->
`Central` can be grouped into two categories: Infrastructure and Application.

### `Central` infrastructure

There may be many flavors of the `Central` infrastructure, but the goal of every variety is to provide a similar setup
for `Central` application deployment. These flavors are different target deployment environments.
For example: GCP and Docker-Desktop. Each `Central` infrastructure should result in the following:

* A Postgres database.
* A Redis cache.
* A Nats messaging cluster.
* An Ingress that can route traffic based on path to Horizon SCMs.
* A Kubernetes cluster to install the `Central` application.
* All the information needed to connect to and leverage the provided infrastructure services.
* A static Nginx HTTP server for serving the UI. The static UI is uploaded as a later step. 

For detailed information of what each template is delivering explore the templates themselves.

### `Central` application

The implementation of the `Central` application is specific to the solution being deployed. This consists of all the SCMs the solution requires. The SCMs are installed via Helm Charts.

The template for application is going to do some information gathering and transformation to take all the info from
the expected info from the `Central` infrastructure and convert it into reusable blocks that can be passed into the Helm Charts.

An application should just point to a Kubernetes cluster via an available kube context. This allows the application definition
to be portable with many flavors of infrastructure or even just different instances of the same infrastructure deployed into different environments.

## What is `Edge`

`Edge` is the portion of a solution that needs to be close to the warehouse hardware. This is meant to be for low latency
responses without necessarily needing to go all the way to `Central` to get an answer. This part of the deployment contains
things like messaging, a cache, and `Edge` Horizon SCMs.

Much like `Central`, `Edge` can be grouped into two overarching categories: Infrastructure and Application. `Edge` follows
the same overall pattern and structure as described above for `Central`.  However, `Edge` will deploy a slightly different set of services.

### `Edge` infrastructure

The `Edge` infrastructure should result in the following:

* A Redis cache.
* A Nats messaging cluster.
* An Ingress that can route traffic based on path to `Edge` and `Central` Horizon SCMs.
* All the information needed to connect to and leverage the provided infrastructure services.
* A Kubernetes cluster to install the `Edge` application.

### `Edge` application

The `Edge` application behaves similarly to the `Central` one. It gathers and transforms data into the format needed by the SCM Helm Charts. This one, unlike `Central`, needs kube context for both the `Edge` and `Central` clusters.

## Terraform template naming scheme

The Terraform templates follow the following these recommended naming schemes:

* Infrastructure—`inf-<target variety>-<description>`
* Application—`hzn-<description>`

For the infrastructure templates there will be more variety as there are plans to support many target deployment environments.

## Deployment order

1. Deploy `Central` Infrastructure.
2. Deploy `Edge` Infrastructure.
3. Deploy `Central` Application.
4. Deploy `Edge` Application.

The above steps assume you have the Terraform configuration correct for all the components you are deploying.

!!! note

    Some configurations may deploy multiple pieces in one Terraform project.


## Setting up your project

<!--  TODO: Clarify the repo statement here, also add a link to the topic that goes through downloading the templates.) -->

After forking this repo for your project, you can run the `update-templates.ps1` script to pull down all the 
available templates to use as a starting point. These templates also contain reference material for setting up a project. 

The `update-templates.ps1` will create this structure in your repository:

<pre>
└── application
    └── templates
        ├── hzn-central
        └── hzn-edge
└── infrastructure
    └── templates
        ├── inf-devk8s-all
        ├── inf-gcp-central
        └── inf-k8s-edge
</pre>

## Customize the Terraform

<!-- TODO: Need to rework this section a bit for custom and override. -->

You can add directories for each environment you want to define under both `application` and `infrastructure`, then copy
the necessary templates for that environment and update them as necessary.

This may result in your repository looking something like this with directories for `prod` and `stage` under the `application` and `infrastructure` directories:

<pre>
└── application
    └── templates
        ├── hzn-central
        └── hzn-edge
    └── prod
        ├── hzn-central
        └── hzn-edge
    └── stage
        ├── hzn-central
        └── hzn-edge
└── infrastructure
    └── templates
        ├── inf-devk8s-all
        ├── inf-gcp-central
        └── inf-k8s-edge
    └── prod
        ├── inf-gcp-central
        └── inf-k8s-edge
    └── stage
        ├── inf-gcp-central
        └── inf-k8s-edge
</pre>

The `prod` environment above uses some but not all the provided Terraform templates. Using this directory structure, you can check in the specifics for each environment (in this example, `prod` and `stage`) and maintain everything in parallel. You can also update the templates in the various environments without overwriting any project specific Terraform configurations found in `templates`.

### Terraform Override files and custom resources

The recommendation for modifications to the template files is using and checking in `Override` files that use the `_override.tf` suffix when naming files.  This is a best practice in Terraform that's outlined in their documentation and also explained below:

* [https://developer.hashicorp.com/terraform/language/files/override](https://developer.hashicorp.com/terraform/language/files/override){:target="_blank"}

By using override files, the standard Terraform template files can be updated in your project (using the `update-templates.ps1` script) as the templates are updated by Dematic Development. This strategy also results in a clear indication of what is customized in your particular solution or customer project.

### Custom resources

If you require **completely new** resources, you can use the `custom_` prefix for files. For example, `custom_<description>.tf`. Using this naming convention helps distinguish between existing Terraform template files and custom Terraform resources for your deployment.

The example below contains the provided standard Terraform template files (`cache.tf`, `messaging.tf`, `variables.tf`), override files (`messaging_override.tf`, `variables_override.tf`), and custom resources (`custom_firewall.tf`):
<pre>
└── infrastructure
    ├──templates
    └── prod
        └── inf-gcp-central
            ├── cache.tf
            ├── custom_firewall.tf
            ├── messaging.tf
            ├── messaging_override.tf
            ├── variables.tf
            └── variables_override.tf
</pre>

For example, there are required values for variables that are specific to a project/solution that **are not** defined in the provided Terraform template files such as `variables.tf`—see line 1 in the `variables.tf` file below. In this case, the `central_kube_config_context` variable doesn't have a default value for the kube context:

`variables.tf`

```hcl linenums="1" hl_lines="1"
variable "central_kube_config_context" {
  type        = string
  description = "The kube context to use for connecting to your Kubernetes cluster. This must be available locally on the system executing the Terraform."
}
```

To address the issue mentioned above and assign a default value for the `central_kube_config_context` variable, you can use an override file named `variables_override.tf` that sets a default kube context of `gke_project_context` for your deployment:

`variables_override.tf`

```hcl linenums="1" hl_lines="2"
variable "central_kube_config_context" {
  default        = "gke_project_context"
}
```

Another example would be to modify a resource to only allow certain traffic ranges with your load balancer. The provided `messaging.tf` template file doesn't specify a traffic range as seen on line 31 below:

`messaging.tf`

```hcl linenums="1" hl_lines="31"
resource "kubernetes_service" "nats" {
  depends_on = [helm_release.nats]

  metadata {
    name      = "nats-nlb"
    namespace = local.central_namespace
    annotations = {
      "networking.gke.io/load-balancer-type" = "External"
    }
    labels = {
      app = "nats"
    }
  }

  spec {
    port {
      name     = "nats"
      port     = "4222"
      protocol = "TCP"
    }

    selector = {
      "app.kubernetes.io/name" : "nats"
    }

    type = "LoadBalancer"
    load_balancer_ip = google_compute_address.nats_ip_address.address

    # Uncomment this and set a list of source cidr ranges to allow access to this external ingress. Leaving this
    # commented out will result in the default allowed range being 0.0.0.0/0
    # load_balancer_source_ranges = ["0.0.0.0/0"]
  }
}
```

To address this issue, you can use an override file to specify a value as seen on line 18 of the `messaging_override.tf` file below:

`messaging_override.tf`

```hcl linenums="1" hl_lines="18"
resource "kubernetes_service" "nats" {
  spec {
    port {
      name     = "nats"
      port     = "4222"
      protocol = "TCP"
    }

    selector = {
      "app.kubernetes.io/name" : "nats"
    }

    type = "LoadBalancer"
    load_balancer_ip = google_compute_address.nats_ip_address.address

    # Uncomment this and set a list of source cidr ranges to allow access to this external ingress. Leaving this
    # commented out will result in the default allowed range being 0.0.0.0/0
   load_balancer_source_ranges = ["192.168.1.1"]
  }
}
```