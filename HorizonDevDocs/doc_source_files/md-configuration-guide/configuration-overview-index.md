<!--  TODO: -->

Configuration information here.

* SCM-specific technical documentation should be tied to the releases of SCMs, GPI can investigate
  putting this documentation in GitLab.
* Principle/overview information about SCMs can stay in the wiki as there will be few changes after
  the initial release.
