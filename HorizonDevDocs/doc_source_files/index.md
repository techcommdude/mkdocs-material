!!! warning

    The documentation on this site is currently a **work-in-progress** for the next Project Horizon release.


This site has Project Horizon documentation for the SAE group with information on deployment, upgrade, configuration, troubleshooting/debugging and other important deployment tips.

Documentation is open to contributions and suggestions from anyone that has a GitLab account. To contribute, you only need to request access to [this project](https://gitlab.dematic.com/gst/docusaurus-static-site){:target="_blank"} in GitLab. Once you have access, you can edit the Markdown source files directly in GitLab. You can also add new Markdown content. For more information on how to contribute, see [Contributing to the documentation](md-getting-started/contributing.md).

![Dematic splash](images/DematicSplash.gif){ loading=lazy }






