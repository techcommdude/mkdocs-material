# Use this script to run a quick and "dirty" build when developing documentation. Use this type of build to quickly test content changes that are not extensive.
# For this script to work, you must have a Python virtual environment named "venv" directly under the top-level directory of this project.
# Run the script in PS with this command: ./MkDocs-build-docs-dirty.ps1

echo "Activating virtual environment.....................................
..........................................................
.............................
"
.\venv\Scripts\Activate

# Navigate to the directory where the mkdocs.yml file is located
cd HorizonDevDocs

# List the details of the current directory
echo "The current directory is.....................................
..........................................................
.................................
"
ls

echo "Building the site..................................
..........................................................
.................................
"
# This is generally much, much faster than mkdocs build --dirty or just mkdocs build
mkdocs build --dirty

#For some reason, this is not any quicker than mkdocs serve.
mkdocs serve --dirtyreload

# This script will build the site and display it locally at this URL:  http://127.0.0.1:8000/horizon-dev-docs/
# Use CTRL+C to to stop the script.
# To run the script, run this command in your terminal window: ./MkDocsBuildSite.ps1
