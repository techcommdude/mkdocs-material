# Use this script to run a full build when committing the files to production. Use this type of build to test comprehensive changes.
# For this script to work, you must have a Python virtual environment named "venv" directly under the top-level directory of this project.
# Run the script in PS with this command: ./MkDocs-build-docs-clean.ps1


echo "Activating virtual environment.....................................
..........................................................
.....................................
"
.\venv\Scripts\Activate

# Navigate to the directory where the mkdocs.yml file is located
cd HorizonDevDocs

# List the details of the current directory
echo "The current directory is.....................................
..........................................................
.................................
"
ls

echo "Building the site..................................
..........................................................
.................................
"
mkdocs build --clean

mkdocs serve --clean

# This script will build the site and display it locally at this URL:  http://127.0.0.1:8000/horizon-dev-docs/
# Use CTRL+c to to stop the script.
# To run the script, run this command in your terminal window: ./MkDocsBuildSite.ps1
